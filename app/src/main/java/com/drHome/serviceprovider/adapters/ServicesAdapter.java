package com.drHome.serviceprovider.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.drHome.serviceprovider.R;
import com.drHome.serviceprovider.models.OrderMedicineModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ServicesAdapter extends RecyclerView.Adapter<ServicesAdapter.ViewHolder> {
    private ArrayList<OrderMedicineModel> orderMedicineModels=new ArrayList<>();
    private Context context;
    private OnItemClick mOnItemClick;

    public ArrayList<OrderMedicineModel> getOrderMedicineModels() {
        return orderMedicineModels;
    }

    public void setOrderMedicineModels(ArrayList<OrderMedicineModel> orderMedicineModels) {
        this.orderMedicineModels = orderMedicineModels;
        notifyDataSetChanged();
    }
//    public NotificationAdapter(ArrayList<NotifcationModel> notifcationModels,Context context, OnItemClick mOnItemClick) {
//        this.mOnItemClick = mOnItemClick;
//        this.context = context;
//        this.notifcationModels = notifcationModels;
//    }


    public ServicesAdapter(Context context, OnItemClick mOnItemClick) {
        this.mOnItemClick = mOnItemClick;
        this.context = context;
    }

    @NonNull
    @Override

    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.service_item, viewGroup, false);
        return new ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        viewHolder.medicineNameTv.setText(orderMedicineModels.get(position).getMedicineName());
        viewHolder.medicineDoseTv.setText(context.getString(R.string.Quantity)+orderMedicineModels.get(position).getQuantity()+" "+context.getString(R.string.Tap));
       // viewHolder.medicineNameTv.setText(orderMedicineModels.get(position).getMedicineName());

    }

    @Override
    public int getItemCount() {

        return orderMedicineModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.medicine_dose_tv)
        TextView medicineDoseTv;

        @BindView(R.id.medicine_name_tv)
        TextView medicineNameTv;

        @BindView(R.id.drug_ic)
        ImageView drugIc;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mOnItemClick.setOnItemClick(getAdapterPosition());
        }
    }

    public interface OnItemClick {
        void setOnItemClick(int position);
    }
}

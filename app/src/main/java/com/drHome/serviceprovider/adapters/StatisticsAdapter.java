package com.drHome.serviceprovider.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.drHome.serviceprovider.R;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;


public class StatisticsAdapter extends RecyclerView.Adapter<StatisticsAdapter.ViewHolder> {
    //   private ArrayList<NotifcationModel> notifcationModels;
    private Context context;
    private OnItemClick mOnItemClick;
    private String newOrders = "",cancelled = "",completed = "",returned = "";

    public void setNewOrders(String newOrders) {
        this.newOrders = newOrders;
    }

    public void setCancelled(String cancelled) {
        this.cancelled = cancelled;
    }

    public void setCompleted(String completed) {
        this.completed = completed;
    }

    public void setReturned(String returned) {
        this.returned = returned;
    }
    //    public NotificationAdapter(ArrayList<NotifcationModel> notifcationModels,Context context, OnItemClick mOnItemClick) {
//        this.mOnItemClick = mOnItemClick;
//        this.context = context;
//        this.notifcationModels = notifcationModels;
//    }


    public StatisticsAdapter(Context context, OnItemClick mOnItemClick) {
        this.context = context;
        this.mOnItemClick = mOnItemClick;
    }

    @NonNull
    @Override

    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.statistics_item, viewGroup, false);
        return new ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {

        switch (position) {
            case 0: {
                viewHolder.constrainView.setBackgroundResource(R.drawable.home_blue_bg);
                viewHolder.countTv.setText(newOrders);
                viewHolder.nameTv.setText(context.getString(R.string.New_Orders));
                Picasso.with(context).load(R.drawable.medicine_ic).into(viewHolder.image);
                return;
            }
            case 1: {
                viewHolder.constrainView.setBackgroundResource(R.drawable.home_orange_bg);
                viewHolder.countTv.setText(cancelled);
                viewHolder.nameTv.setText(context.getString(R.string.Cancelled));
                Picasso.with(context).load(R.drawable.cancel_ic).into(viewHolder.image);

                return;
            }
            case 2: {
                viewHolder.constrainView.setBackgroundResource(R.drawable.home_green_bg);
                viewHolder.countTv.setText(completed);
                viewHolder.nameTv.setText(context.getString(R.string.Completed));
                Picasso.with(context).load(R.drawable.done_ic).into(viewHolder.image);

                return;
            }
            case 3: {
                viewHolder.constrainView.setBackgroundResource(R.drawable.home_purple_bg);
                viewHolder.countTv.setText(returned);
                viewHolder.nameTv.setText(context.getString(R.string.Returned_Order));
                Picasso.with(context).load(R.drawable.delivery_ic).into(viewHolder.image);

                return;
            }
        }


    }

    @Override
    public int getItemCount() {

        //  return notifcationModels.size();


        return 4;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.constrain_view)
        ConstraintLayout constrainView;


        @BindView(R.id.count_tv)
        TextView countTv;


        @BindView(R.id.name_tv)
        TextView nameTv;


        @BindView(R.id.image)
        ImageView image;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mOnItemClick.setOnItemClick(getAdapterPosition());
        }
    }

    public interface OnItemClick {
        void setOnItemClick(int position);
    }
}

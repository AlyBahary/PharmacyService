package com.drHome.serviceprovider.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.drHome.serviceprovider.R;
import com.drHome.serviceprovider.models.OrderModel;
import com.drHome.serviceprovider.utils.Config;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class OrderHistoryAdapter extends RecyclerView.Adapter<OrderHistoryAdapter.ViewHolder> {
    private ArrayList<OrderModel> newOrderModels = new ArrayList<>();
    private Context context;
    private OnItemClick mOnItemClick;
    private int pageNumber = 0;

    public ArrayList<OrderModel> getNewOrderModels() {
        return newOrderModels;
    }

    public void searchByID(Integer id) {
        Boolean found = false;
        for (int i = 0; i < newOrderModels.size(); i++) {
            if (newOrderModels.get(i).getId().equals(id)) {
                OrderModel orderModel = newOrderModels.get(i);
                newOrderModels.clear();
                newOrderModels.clone();
                newOrderModels.add(orderModel);
                notifyDataSetChanged();
                found=true;
            }
        }
        if (!found) {
            Toast.makeText(context, "" + context.getString(R.string.there_is_no_order_this_id), Toast.LENGTH_SHORT).show();
        }
    }

    public void addToNewOrderModels(ArrayList<OrderModel> newOrderModels) {
        this.newOrderModels.addAll(newOrderModels);
        notifyDataSetChanged();
    }

    public void setNewOrderModels(ArrayList<OrderModel> newOrderModels) {
        this.newOrderModels = (newOrderModels);
        notifyDataSetChanged();
    }


    public OrderHistoryAdapter(Context context, OnItemClick mOnItemClick) {
        this.mOnItemClick = mOnItemClick;
        this.context = context;
    }

    @NonNull
    @Override

    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.order_details_item, viewGroup, false);
        return new ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {

        viewHolder.orderNumTv.setText(newOrderModels.get(position).getId() + "");
        viewHolder.orderDateTv.setText(newOrderModels.get(position).getDate().replace("00:00:00", "") + "");
        viewHolder.nameTv.setText(newOrderModels.get(position).getUserData().getFullName() + "");
        viewHolder.phoneTv.setText(newOrderModels.get(position).getUserData().getMobileNumber() + "");
        viewHolder.orderStatusTv.setText(newOrderModels.get(position).getStatus());

        if (newOrderModels.get(position).getUserData().getProfilePath() != null) {
            Glide.with(context).load(Config.BaseUrl + newOrderModels.get(position).getUserData().getProfilePath())
                    .apply(RequestOptions.circleCropTransform().placeholder(R.drawable.avatar))
                    .into(viewHolder.avatar_img);

        }
        if (newOrderModels.get(position).getStatus().equals("Completed")) {
            viewHolder.orderStatusTv.setTextColor(context.getResources().getColor(R.color.mint_green));
            viewHolder.orderStatusTv.setBackgroundResource(R.drawable.green_rounded_border_bg);
            Glide.with(context).load(R.drawable.check_circle_ic).into(viewHolder.state2Img);
            Glide.with(context).load(R.drawable.check_circle_ic).into(viewHolder.state3Img);

        } else if (newOrderModels.get(position).getStatus().equals("New")) {
            viewHolder.orderStatusTv.setTextColor(context.getResources().getColor(R.color.mint_green));
            viewHolder.orderStatusTv.setBackgroundResource(R.drawable.green_rounded_border_bg);
            Glide.with(context).load(R.drawable.empty_circle_ic).into(viewHolder.state2Img);
            Glide.with(context).load(R.drawable.empty_circle_ic).into(viewHolder.state3Img);

        } else if (newOrderModels.get(position).getStatus().equals("Approved")) {
            viewHolder.orderStatusTv.setTextColor(context.getResources().getColor(R.color.mint_green));
            viewHolder.orderStatusTv.setBackgroundResource(R.drawable.green_rounded_border_bg);
            if (newOrderModels.get(position).getDeliveryMan()) {
                Glide.with(context).load(R.drawable.check_circle_ic).into(viewHolder.state2Img);
            } else {
                Glide.with(context).load(R.drawable.empty_circle_ic).into(viewHolder.state2Img);
            }
            Glide.with(context).load(R.drawable.empty_circle_ic).into(viewHolder.state3Img);

        } else if (newOrderModels.get(position).getStatus().equals("Cancelled")) {
            viewHolder.orderStatusTv.setTextColor(context.getResources().getColor(R.color.red));
            viewHolder.orderStatusTv.setTextColor(context.getResources().getColor(R.color.red));
            viewHolder.orderStatusTv.setBackgroundResource(R.drawable.red_rounded_border_bg);

            Glide.with(context).load(R.drawable.cancel_red_cirlce_ic).into(viewHolder.state2Img);
            Glide.with(context).load(R.drawable.cancel_red_cirlce_ic).into(viewHolder.state3Img);

        } else if (newOrderModels.get(position).getStatus().equals("Rejected")) {
            viewHolder.orderStatusTv.setTextColor(context.getResources().getColor(R.color.red));
            viewHolder.orderStatusTv.setTextColor(context.getResources().getColor(R.color.red));
            viewHolder.orderStatusTv.setBackgroundResource(R.drawable.red_rounded_border_bg);

            Glide.with(context).load(R.drawable.cancel_red_cirlce_ic).into(viewHolder.state3Img);
            Glide.with(context).load(R.drawable.cancel_red_cirlce_ic).into(viewHolder.state3Img);

        } else {
            viewHolder.orderStatusTv.setTextColor(context.getResources().getColor(R.color.red));
            viewHolder.orderStatusTv.setBackgroundResource(R.drawable.red_rounded_border_bg);
            if (newOrderModels.get(position).getDeliveryMan()) {
                Glide.with(context).load(R.drawable.check_circle_ic).into(viewHolder.state2Img);

            } else {
                Glide.with(context).load(R.drawable.cancel_red_cirlce_ic).into(viewHolder.state2Img);
            }
            if (newOrderModels.get(position).getReceived()) {
                Glide.with(context).load(R.drawable.check_circle_ic).into(viewHolder.state3Img);

            } else {
                Glide.with(context).load(R.drawable.cancel_red_cirlce_ic).into(viewHolder.state3Img);
            }
        }


    }

    @Override
    public int getItemCount() {

        //  return notifcationModels.size();


        return newOrderModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.order_status_tv)
        TextView orderStatusTv;

        @BindView(R.id.state2_img)
        ImageView state2Img;

        @BindView(R.id.state3_img)
        ImageView state3Img;

        @BindView(R.id.order_num_tv)
        TextView orderNumTv;

        @BindView(R.id.order_date_tv)
        TextView orderDateTv;

        @BindView(R.id.name_tv)
        TextView nameTv;

        @BindView(R.id.phone_tv)
        TextView phoneTv;

        @BindView(R.id.call_ic)
        ImageView callIc;

        @BindView(R.id.avatar_img)
        ImageView avatar_img;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mOnItemClick.setOnItemClick(getAdapterPosition());
        }
    }

    public interface OnItemClick {
        void setOnItemClick(int position);
    }
}

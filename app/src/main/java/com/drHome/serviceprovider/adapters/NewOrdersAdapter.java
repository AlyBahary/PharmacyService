package com.drHome.serviceprovider.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.drHome.serviceprovider.R;
import com.drHome.serviceprovider.models.OrderModel;
import com.drHome.serviceprovider.utils.Config;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class NewOrdersAdapter extends RecyclerView.Adapter<NewOrdersAdapter.ViewHolder> {
    private ArrayList<OrderModel> orderModels = new ArrayList<>();
    private Context context;
    private OnItemClick mOnItemClick;
    int pageNumber = 0;

    public void setOrderModels(ArrayList<OrderModel> orderModels, int pageNumber) {
        if (this.pageNumber == pageNumber) {
            this.orderModels = orderModels;
        } else {
            this.orderModels.addAll(orderModels);
        }
        pageNumber++;
        notifyDataSetChanged();
    }

    public ArrayList<OrderModel> getOrderModels() {
        return orderModels;
    }
    //    public NotificationAdapter(ArrayList<NotifcationModel> notifcationModels,Context context, OnItemClick mOnItemClick) {
//        this.mOnItemClick = mOnItemClick;
//        this.context = context;
//        this.notifcationModels = notifcationModels;
//    }


    public NewOrdersAdapter(Context context, OnItemClick mOnItemClick) {
        this.mOnItemClick = mOnItemClick;
        this.context = context;
    }

    @NonNull
    @Override

    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.order_item, viewGroup, false);
        return new ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {

        viewHolder.nameTv.setText(orderModels.get(position).getUserData().getFullName());
        viewHolder.dateTv.setText(orderModels.get(position).getDate().replace("00:00:00", ""));
        if (orderModels.get(position).getUserData().getProfilePath() != null) {
            Glide.with(context).load(Config.BaseUrl + orderModels.get(position).getUserData().getProfilePath())
                    .apply(RequestOptions.circleCropTransform().placeholder(R.drawable.avatar))
                    .into(viewHolder.avatarImg);

        }

    }

    @Override
    public int getItemCount() {

        //  return notifcationModels.size();


        return orderModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


        @BindView(R.id.avatar_img)
        ImageView avatarImg;

        @BindView(R.id.name_tv)
        TextView nameTv;

        @BindView(R.id.date_tv)
        TextView dateTv;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mOnItemClick.setOnItemClick(getAdapterPosition());
        }
    }

    public interface OnItemClick {
        void setOnItemClick(int position);
    }
}

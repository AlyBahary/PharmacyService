package com.drHome.serviceprovider.viewmodel;

import android.util.Log;

import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.auth0.android.jwt.JWT;
import com.drHome.serviceprovider.R;
import com.drHome.serviceprovider.models.UserModel;
import com.drHome.serviceprovider.repository.Repository;
import com.drHome.serviceprovider.utils.AndroidApplication;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.TypeAdapter;

import java.io.IOException;
import java.util.HashMap;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.HttpException;


public class AuthViewModel extends ViewModel {

    private Repository repository;
    MutableLiveData<JsonObject> loginMutableLiveData;
    MutableLiveData<JsonObject> loginErrorMutableLiveData;

    MutableLiveData<JsonObject> generateCodeMutableLiveData;
    MutableLiveData<JsonObject> generateCodeErrorMutableLiveData;

    MutableLiveData<JsonObject> changePasswordMutableLiveData;
    MutableLiveData<JsonObject> changePasswordErrorMutableLiveData;

    MutableLiveData<UserModel> userProfileMutableLiveData;
    MutableLiveData<JsonObject> userProfileErrorMutableLiveData;

    MutableLiveData<JsonObject> addSupportMessageMutableLiveData;
    MutableLiveData<JsonObject> addSupportMessageErrorMutableLiveData;

    public MutableLiveData<JsonObject> getGenerateCodeMutableLiveData() {
        return generateCodeMutableLiveData;
    }

    public MutableLiveData<JsonObject> getGenerateCodeErrorMutableLiveData() {
        return generateCodeErrorMutableLiveData;
    }

    public MutableLiveData<UserModel> getUserProfileMutableLiveData() {
        return userProfileMutableLiveData;
    }

    public MutableLiveData<JsonObject> getUserProfileErrorMutableLiveData() {
        return userProfileErrorMutableLiveData;
    }

    public MutableLiveData<JsonObject> getAddSupportMessageMutableLiveData() {
        return addSupportMessageMutableLiveData;
    }

    public MutableLiveData<JsonObject> getAddSupportMessageErrorMutableLiveData() {
        return addSupportMessageErrorMutableLiveData;
    }

    public MutableLiveData<JsonObject> getLoginMutableLiveData() {
        return loginMutableLiveData;
    }

    public MutableLiveData<JsonObject> getLoginErrorMutableLiveData() {
        return loginErrorMutableLiveData;
    }

    public MutableLiveData<JsonObject> getChangePasswordMutableLiveData() {
        return changePasswordMutableLiveData;
    }

    public MutableLiveData<JsonObject> getChangePasswordErrorMutableLiveData() {
        return changePasswordErrorMutableLiveData;
    }

    @ViewModelInject
    public AuthViewModel(Repository repository) {
        this.repository = repository;
    }


    public void login(String username
            , String password
    ) {
        loginMutableLiveData = new MutableLiveData<JsonObject>();
        loginErrorMutableLiveData = new MutableLiveData<JsonObject>();
        repository.login(username, password)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                            loginMutableLiveData.setValue(result);
                            },
                        error -> {
                            convertErrorBodyToJson(error, loginErrorMutableLiveData);
                        });

    }

    public void generateOtpCode(String email) {
        generateCodeMutableLiveData = new MutableLiveData<>();
        generateCodeErrorMutableLiveData = new MutableLiveData<>();
        repository.generateOtpCode(email)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                            generateCodeMutableLiveData.setValue(result);
                        },
                        error -> {
                            convertErrorBodyToJson(error, generateCodeErrorMutableLiveData);
                        });

    }

    public void changeforgetPassword(HashMap<String,String> dto) {
        generateCodeMutableLiveData = new MutableLiveData<>();

        repository.changeforgetPassword(dto)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                            generateCodeMutableLiveData.setValue(result);
                        },
                        error -> {
                            convertErrorBodyToJson(error, generateCodeMutableLiveData);
                        });

    }

    public void getAccountDetails(String userId) {
        userProfileMutableLiveData = new MutableLiveData<>();
        userProfileErrorMutableLiveData = new MutableLiveData<>();

        repository.getAccountDetails(userId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                            userProfileMutableLiveData.setValue(result);
                        },
                        error -> {
                            convertErrorBodyToJson(error, userProfileErrorMutableLiveData);
                        });

    }

    public void updateAccountDetails(String userId, UserModel userModel) {
        userProfileMutableLiveData = new MutableLiveData<>();
        userProfileErrorMutableLiveData = new MutableLiveData<>();

        repository.updateAccountDetails(userId, userModel)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                            Log.d("TAG", "updateAccountDetails: DOne");
                            userProfileMutableLiveData.setValue(result);
                        },
                        error -> {
                            Log.d("TAG", "updateAccountDetails: Error");

                            convertErrorBodyToJson(error, userProfileErrorMutableLiveData);
                        });

    }

    public void changePassword(HashMap<String, String> dto) {
        changePasswordMutableLiveData = new MutableLiveData<>();
        changePasswordErrorMutableLiveData = new MutableLiveData<>();
        repository.changePassword(dto)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                            changePasswordMutableLiveData.setValue(result);
                        },
                        error -> {
                            convertErrorBodyToJson(error, changePasswordMutableLiveData);
                        });

    }

    public void addSupportMessage(HashMap<String, String> dto) {
        addSupportMessageMutableLiveData = new MutableLiveData<>();
        addSupportMessageErrorMutableLiveData = new MutableLiveData<>();
        repository.addSupportMessage(dto)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                            Log.d("TAG", "addSupportMessage: ----");

                            addSupportMessageMutableLiveData.setValue(result);
                        },
                        error -> {
                            convertErrorBodyToJson(error, addSupportMessageErrorMutableLiveData);
                        });

    }


    private void convertErrorBodyToJson(Throwable error, MutableLiveData<JsonObject> errorMutableLiveData) {

        Log.d("TAG", "convertErrorBodyToJson: 1");
        if (error instanceof HttpException) {
            ResponseBody body = ((HttpException) error).response().errorBody();
            Gson gson = new Gson();
            TypeAdapter<JsonObject> adapter = gson.getAdapter
                    (JsonObject
                            .class);
            try {
                Log.d("TAG", "convertErrorBodyToJson: 2");

                errorMutableLiveData.setValue(adapter.fromJson(body.string()));
                // Logger.i(TAG, "Error:" + errorParser.getError());

            } catch (IOException e) {
                e.printStackTrace();
                Log.d("TAG", "convertErrorBodyToJson: "+e.getLocalizedMessage());

            }
        } else if (error instanceof IOException) {
            Log.d("TAG", "convertErrorBodyToJson: ()");

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("error", AndroidApplication.getAppContext().getString(R.string.Please_check_internet_connection));
            errorMutableLiveData.setValue(jsonObject);

        }
    }

}

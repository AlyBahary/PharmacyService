package com.drHome.serviceprovider.viewmodel;

import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.drHome.serviceprovider.models.DashBoardDataModel;
import com.drHome.serviceprovider.models.NewOrderModel;
import com.drHome.serviceprovider.models.OrderDatailsResponseModel;
import com.drHome.serviceprovider.repository.Repository;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.TypeAdapter;

import java.io.IOException;
import java.util.HashMap;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.HttpException;


public class HomeViewModel extends ViewModel {

    private Repository repository;
    MutableLiveData<DashBoardDataModel> dashBoardDataModelMutableLiveData;
    MutableLiveData<JsonObject> dashBoardDataErrorModelMutableLiveData;
    //
    MutableLiveData<NewOrderModel> newOrderModelMutableLiveData;
    MutableLiveData<JsonObject> newOrderErrorModelMutableLiveData;
    //histiory fragment
    MutableLiveData<NewOrderModel> orderModelMutableLiveData;
    MutableLiveData<JsonObject> orderErrorModelMutableLiveData;
    //
    MutableLiveData<OrderDatailsResponseModel> orderDatailsResponseModelMutableLiveData;
    MutableLiveData<JsonObject> orderDatailsErrorResponseModelMutableLiveData;
    //
    MutableLiveData<JsonObject> orderStatusMutableLiveData;
    MutableLiveData<JsonObject> orderStatusErrorMutableLiveData;

    public MutableLiveData<NewOrderModel> getOrderModelMutableLiveData() {
        return orderModelMutableLiveData;
    }

    public MutableLiveData<JsonObject> getOrderErrorModelMutableLiveData() {
        return orderErrorModelMutableLiveData;
    }

    public MutableLiveData<JsonObject> getOrderStatusMutableLiveData() {
        return orderStatusMutableLiveData;
    }

    public MutableLiveData<JsonObject> getOrderStatusErrorMutableLiveData() {
        return orderStatusErrorMutableLiveData;
    }

    public MutableLiveData<JsonObject> getDashBoardDataErrorModelMutableLiveData() {
        return dashBoardDataErrorModelMutableLiveData;
    }

    public MutableLiveData<OrderDatailsResponseModel> getOrderDatailsResponseModelMutableLiveData() {
        return orderDatailsResponseModelMutableLiveData;
    }

    public MutableLiveData<JsonObject> getOrderDatailsErrorResponseModelMutableLiveData() {
        return orderDatailsErrorResponseModelMutableLiveData;
    }

    public MutableLiveData<NewOrderModel> getNewOrderModelMutableLiveData() {
        return newOrderModelMutableLiveData;
    }

    public MutableLiveData<JsonObject> getNewOrderErrorModelMutableLiveData() {
        return newOrderErrorModelMutableLiveData;
    }

    public MutableLiveData<DashBoardDataModel> getDashBoardDataModelMutableLiveData() {
        return dashBoardDataModelMutableLiveData;
    }


    public MutableLiveData<JsonObject> getDashBoardDataERRORModelMutableLiveData() {
        return dashBoardDataErrorModelMutableLiveData;
    }

    @ViewModelInject
    public HomeViewModel(Repository repository) {
        this.repository = repository;
    }


    public void getDashBoardData(String branchId) {
        dashBoardDataModelMutableLiveData = new MutableLiveData<DashBoardDataModel>();
        dashBoardDataErrorModelMutableLiveData = new MutableLiveData<JsonObject>();
        repository.getDashBoardData(branchId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                            dashBoardDataModelMutableLiveData.setValue(result);
                        },
                        error -> {
                            convertErrorBodyToJson(error, getDashBoardDataERRORModelMutableLiveData());
                        });

    }

    public void saveUserToken() {
        repository.saveUserToken()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                        },
                        error -> {
                        });

    }

    public void getNewOrders(HashMap<String, String> dto, String pageNumber) {
        newOrderModelMutableLiveData = new MutableLiveData<NewOrderModel>();
        newOrderErrorModelMutableLiveData = new MutableLiveData<JsonObject>();
        repository.getNewOrders(dto, pageNumber)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                            newOrderModelMutableLiveData.setValue(result);
                        },
                        error -> {
                            convertErrorBodyToJson(error, newOrderErrorModelMutableLiveData);
                        });

    }

    public void getOrders(HashMap<String, String> dto, String pageNumber) {
        orderModelMutableLiveData = new MutableLiveData<NewOrderModel>();
        orderErrorModelMutableLiveData = new MutableLiveData<JsonObject>();
        repository.getNewOrders(dto, pageNumber)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                            orderModelMutableLiveData.setValue(result);
                        },
                        error -> {
                            convertErrorBodyToJson(error, orderErrorModelMutableLiveData);
                        });

    }

    public void getOrderDetails(String orderId) {
        orderDatailsResponseModelMutableLiveData = new MutableLiveData<OrderDatailsResponseModel>();
        orderDatailsErrorResponseModelMutableLiveData = new MutableLiveData<JsonObject>();
        repository.getOrderDetails(orderId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                            orderDatailsResponseModelMutableLiveData.setValue(result);
                        },
                        error -> {
                            convertErrorBodyToJson(error, orderDatailsErrorResponseModelMutableLiveData);
                        });

    }

    public void approveRequest(String orderId) {
        orderStatusMutableLiveData = new MutableLiveData<JsonObject>();
        orderStatusErrorMutableLiveData = new MutableLiveData<JsonObject>();
        repository.approveOrder(orderId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                            orderStatusMutableLiveData.setValue(result);
                        },
                        error -> {
                            convertErrorBodyToJson(error, orderStatusErrorMutableLiveData);
                        });

    }

    public void rejectRequest(String orderId, String note) {
        orderStatusMutableLiveData = new MutableLiveData<JsonObject>();
        orderStatusErrorMutableLiveData = new MutableLiveData<JsonObject>();

        repository.rejectOrder(orderId, note)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                            orderStatusMutableLiveData.setValue(result);
                        },
                        error -> {
                            convertErrorBodyToJson(error, orderStatusErrorMutableLiveData);
                        });
    }

    public void updateStatus(String orderId, String status,String comment) {
        orderStatusMutableLiveData = new MutableLiveData<JsonObject>();
        orderStatusErrorMutableLiveData = new MutableLiveData<JsonObject>();

        repository.updateOrderStatus(orderId, status,comment)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                            orderStatusMutableLiveData.setValue(result);
                        },
                        error -> {
                            convertErrorBodyToJson(error, orderStatusErrorMutableLiveData);
                        });
    }


    private void convertErrorBodyToJson(Throwable error, MutableLiveData<JsonObject> errorMutableLiveData) {
        if (error instanceof HttpException) {
            ResponseBody body = ((HttpException) error).response().errorBody();
            Gson gson = new Gson();
            TypeAdapter<JsonObject> adapter = gson.getAdapter
                    (JsonObject
                            .class);
            try {
                errorMutableLiveData.setValue(adapter.fromJson(body.string()));
                // Logger.i(TAG, "Error:" + errorParser.getError());

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

}

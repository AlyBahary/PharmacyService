package com.drHome.serviceprovider.utils;

public class Config {
    //https://api.drathome.com.sa/doctorhome/
    //http://drhometst.beetleware.com/doctorhome/
    public static final String BaseUrl = "http://drhometst.beetleware.com/doctorhome/";
    public static final String PREFS_NAME = "MyPrefsConfg";
    public static final String CONTENT_TYPE = "application/x-www-form-urlencoded";
    public static final String ID ="ID" ;
    public static final String BRANCH_ID ="BRANCH_ID" ;
    public static final String USER_NAME ="USER_NAME" ;
    public static final String USER_ID ="USER_ID" ;
    public static final String ACCESS_TOKEN ="ACCESS_TOKEN" ;
    public static final String CODE ="OTP_CODE" ;
    public static final String OTPNEWPASSWORD ="OTPNEWPASSWORD" ;
    public static final String APPLANGUAGE ="appLanguage" ;


    public static final String FIREBASE_TOKEN ="FIREBASE_TOKEN" ;
    public static final String USER_TYPE ="USER_TYPE" ;
    public static final String EMAIL ="EMAIL" ;
}

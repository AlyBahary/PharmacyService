package com.drHome.serviceprovider.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.util.DisplayMetrics;

import androidx.multidex.MultiDex;

import com.drHome.serviceprovider.BuildConfig;
import com.onesignal.OneSignal;

import java.util.Locale;

import dagger.hilt.android.HiltAndroidApp;
import timber.log.Timber;

@HiltAndroidApp
public class AndroidApplication extends Application {

    //key path  : /Applications/Android Studio.app/Contents/bin/key
    private static Context mContext;

    private static AndroidApplication instance;

    private static final String ONESIGNAL_APP_ID = "c0100340-9219-4c2e-8d7a-6f5705f6b8ef";

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = getApplicationContext();

        instance = this;

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }

        // Enable verbose OneSignal logging to debug issues if needed.
        OneSignal.setLogLevel(OneSignal.LOG_LEVEL.VERBOSE, OneSignal.LOG_LEVEL.NONE);

        // OneSignal Initialization
        OneSignal.initWithContext(this);
        OneSignal.setAppId(ONESIGNAL_APP_ID);

    }

    public static Context getAppContext() {
        return mContext;
    }

    public static AndroidApplication getInstance() {
        return instance;
    }

    public static boolean hasNetwork() {
        return getInstance().checkIfHasNetwork();
    }

    public boolean checkIfHasNetwork() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        @SuppressLint("MissingPermission")
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }



    public static boolean setAppLocale(String lang) {
        if (lang != null) {
            SharedPreferences sharedPreferences = AndroidApplication.getAppContext().getSharedPreferences(Config.PREFS_NAME, Context.MODE_PRIVATE);
            SharedPreferences.Editor sharedPrefEditor = sharedPreferences.edit();
            sharedPrefEditor.putString("appLanguage", lang);
            sharedPrefEditor.apply();
            sharedPrefEditor.commit();

            Locale myLocale = new Locale(lang);
            Resources res = AndroidApplication.getAppContext().getResources();
            DisplayMetrics dm = res.getDisplayMetrics();
            Configuration conf = res.getConfiguration();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                conf.setLocale(myLocale);
            } else {
                conf.locale = myLocale;
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                conf.setLayoutDirection(myLocale);
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                AndroidApplication.getAppContext().createConfigurationContext(conf);
            } else {
                res.updateConfiguration(conf, dm);
            }

            return true;
        }
        return false;

    }

}
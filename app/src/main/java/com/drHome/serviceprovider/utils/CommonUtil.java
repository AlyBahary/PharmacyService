package com.drHome.serviceprovider.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.drHome.serviceprovider.R;

import java.io.FileDescriptor;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;


;


public class CommonUtil {

    public static void makeDefaultLocaleToArabic(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(Config.PREFS_NAME, 0);
        AndroidApplication.setAppLocale(sharedPref.getString(Config.APPLANGUAGE, "ar"));
    }

    public static SharedPreferences commonSharedPref() {
        SharedPreferences pref = AndroidApplication.getAppContext().getSharedPreferences(Config.PREFS_NAME, 0);
        return pref;
    }

    public static String getDataFromSharedPref(String key) {
        SharedPreferences pref = AndroidApplication.getAppContext().getSharedPreferences(Config.PREFS_NAME, 0);
        return pref.getString(key, "");
    }

    public static String getCurrentLang() {
        SharedPreferences pref = AndroidApplication.getAppContext().getSharedPreferences(Config.PREFS_NAME, 0);
        return pref.getString(Config.APPLANGUAGE, "en");
    }

    public static void addToSharedPref(String key, String value) {
        SharedPreferences sharedpreferences = AndroidApplication.getAppContext().getSharedPreferences(Config.PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static void logout() {
        SharedPreferences pref = AndroidApplication.getAppContext().getSharedPreferences(Config.PREFS_NAME, 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.remove(Config.ACCESS_TOKEN);
        editor.apply();
        editor.commit();
    }

    public static String readIt(InputStream stream, long len) throws IOException {
        int n = 0;
        char[] buffer = new char[1024 * 4];
        InputStreamReader reader = new InputStreamReader(stream, "UTF8");
        StringWriter writer = new StringWriter();
        while (-1 != (n = reader.read(buffer))) writer.write(buffer, 0, n);
        return writer.toString();
    }

    public static void showProgressDialog(ProgressDialog progressDialog, String loadingMessage) {
        try {
            if (progressDialog != null) {
                progressDialog.setTitle("");
                //   progressDialog.setTitle(AndroidApplication.getAppContext().getString(R.string.loading_string));
                progressDialog.setMessage(loadingMessage);
                progressDialog.setCancelable(false);
                progressDialog.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void dismissProgressDialog(ProgressDialog progressDialog) {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }


    public static CharSequence[] arrayListToArrayOfCharSequenceItems(ArrayList<String> items) {
        CharSequence[] listOfItems = new CharSequence[items.size()];
        for (int i = 0; i < items.size(); i++) {
            listOfItems[i] = items.get(i);
        }
        return listOfItems;
    }

    public static boolean validateEmail(String email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }


    public static boolean validatePhoneNumber(String phoneNumber) {
        Pattern pattern =
                Pattern.compile("^(009665|9665|\\+9665|05|5)(5|0|3|6|4|9|1|8|7)([0-9]{7})$");

        return pattern.matcher(phoneNumber).matches();
    }

    private static Bitmap getBitmapFromUri(Context context, Uri uri) throws IOException {
        ParcelFileDescriptor parcelFileDescriptor =
                context.getContentResolver().openFileDescriptor(uri, "r");
        FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
        Bitmap image = BitmapFactory.decodeFileDescriptor(fileDescriptor);
        parcelFileDescriptor.close();
        return image;
    }

    public static String dateFromServer(String date) {
        String dateFormat = "yyyy-MM-dd";
        String toDateFormat = "dd MMMM yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat, new Locale("en"));
        SimpleDateFormat sdfTo = new SimpleDateFormat(toDateFormat, new Locale("en"));

        try {
            return sdfTo.format(sdf.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String dateFullFromServerWithTime(String date) {
        String dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZ";
        String toDateFormat = "dd MMMM yyyy 'at' hh:mm a";
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat, new Locale("en"));
        SimpleDateFormat sdfTo = new SimpleDateFormat(toDateFormat, new Locale("en"));

        try {
            return sdfTo.format(sdf.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String dateDaysAndTimeFromServerWithTime(String date) {
        String dateFormat = "yyyy-MM-dd HH:mm";
        String toDateFormat = "dd MMMM yyyy 'at' hh:mm a";
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat, new Locale("en"));
        SimpleDateFormat sdfTo = new SimpleDateFormat(toDateFormat, new Locale("en"));

        try {
            return sdfTo.format(sdf.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String dateDaysAndTimeFromServerWithTimeFull(String date) {
        String dateFormat = "yyyy-MM-dd HH:mm a";
        String toDateFormat = "dd MMMM yyyy 'at' hh:mm a";
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat, new Locale("en"));
        SimpleDateFormat sdfTo = new SimpleDateFormat(toDateFormat, new Locale("en"));

        try {
            return sdfTo.format(sdf.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String dateFullToServer(Date date) {
        String dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZ";
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat, new Locale("en"));

        return sdf.format(date);
    }

    public static long secondsBetweenNowAndServerDate(String serverDate) {
        String dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZ";
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat, new Locale("en"));
        Calendar now = Calendar.getInstance();
        Calendar serverTime = Calendar.getInstance();
        try {
            serverTime.setTime(sdf.parse(serverDate));
            long diffInMs = now.getTimeInMillis() - serverTime.getTimeInMillis();
            return TimeUnit.MILLISECONDS.toSeconds(diffInMs);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return (30 * 60) - 1;
    }

    public static void internetConnectionToast() {
        Toast.makeText(AndroidApplication.getAppContext(), "تحقق من اتصال الانترنت", Toast.LENGTH_SHORT).show();
    }

    public static String getCompleteAddressString(Context context, double LATITUDE, double LONGITUDE) {
        Geocoder geocoder;
        String strAdd = "";
        Locale loc = new Locale("ar");

        geocoder = new Geocoder(context, loc);

        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");

                for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
            } else {
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return strAdd;
    }

    public static boolean isValidEmail(String target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public void printTextLong(String text) {
        Toast.makeText(AndroidApplication.getAppContext(), "" + text, Toast.LENGTH_LONG).show();
    }

    public void printTextShort(String text) {
        Toast.makeText(AndroidApplication.getAppContext(), "" + text, Toast.LENGTH_SHORT).show();
    }

    public void longGreenCustomToast(String text) {
        Toast toast = Toast.makeText(AndroidApplication.getAppContext(), text, Toast.LENGTH_LONG);
        View toastView = toast.getView(); // This'll return the default View of the Toast.
        /* And now you can get the TextView of the default View of the Toast. */
        TextView toastMessage = (TextView) toastView.findViewById(android.R.id.message);
        toastMessage.setTextSize(20);
        toastMessage.setTextColor(Color.WHITE);
        toastMessage.setGravity(Gravity.CENTER);
        toastView.setBackgroundResource(R.drawable.green_rounded_bg);
        toast.show();
    }

    public void longOrangeCustomToast(String text) {
        Toast toast = Toast.makeText(AndroidApplication.getAppContext(), text, Toast.LENGTH_LONG);
        View toastView = toast.getView(); // This'll return the default View of the Toast.
        /* And now you can get the TextView of the default View of the Toast. */
        TextView toastMessage = (TextView) toastView.findViewById(android.R.id.message);
        toastMessage.setTextSize(20);
        toastMessage.setTextColor(Color.WHITE);
        toastMessage.setGravity(Gravity.CENTER);
        toastView.setBackgroundResource(R.drawable.orange_rounded_bg);
        toast.show();
    }

    public void longRedCustomToast(String text) {
        Toast toast = Toast.makeText(AndroidApplication.getAppContext(), text, Toast.LENGTH_LONG);
        View toastView = toast.getView(); // This'll return the default View of the Toast.
        /* And now you can get the TextView of the default View of the Toast. */
        TextView toastMessage = (TextView) toastView.findViewById(android.R.id.message);
        toastMessage.setTextSize(20);
        toastMessage.setTextColor(Color.WHITE);
        toastMessage.setGravity(Gravity.CENTER);
        toastView.setBackgroundResource(R.drawable.red_rounded_bg);
        toast.show();
    }

    public static int calculateNoOfColumns(Context context, float columnWidthDp) { // For example columnWidthdp=180
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        float screenWidthDp = displayMetrics.widthPixels / displayMetrics.density;
        Log.d("TAG", "calculateNoOfColumns: "+screenWidthDp);
        int noOfColumns = (int) (screenWidthDp / (columnWidthDp + 0.5)); // +0.5 for correct rounding to int.

        return noOfColumns;
    }
//    public static void internetConnectionToast(){
//        Toast.makeText(AndroidApplication.getAppContext(), ""+ AndroidApplication.getAppContext().getString(R.string.Please_check_your_internet_Connection), Toast.LENGTH_SHORT).show();
//    }
//
//    public static void pleaseTryAgain(){
//        Toast.makeText(AndroidApplication.getAppContext(), ""+ AndroidApplication.getAppContext().getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
//    }
}

package com.drHome.serviceprovider.network;


import com.drHome.serviceprovider.models.DashBoardDataModel;
import com.drHome.serviceprovider.models.NewOrderModel;
import com.drHome.serviceprovider.models.OrderDatailsResponseModel;
import com.drHome.serviceprovider.models.UserModel;
import com.google.gson.JsonObject;

import java.util.HashMap;

import io.reactivex.rxjava3.core.Observable;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiService {


    @GET("api/pharmacy/branch-interface/dashboard")
    Observable<DashBoardDataModel> getDashBoardData(@Query("branchId") String branchId);

    @POST("api/pharmacy/pharmacy-interface/orders/{pageNumber}")
    Observable<NewOrderModel> getNewOrders(@Body HashMap<String, String> dto, @Path("pageNumber") String pageNumber);

    @GET("api/pharmacy/pharmacy-interface/orders/{order_id}")
    Observable<OrderDatailsResponseModel> getOrderDetails(@Path("order_id") String orderId);

    @PUT("api/pharmacy/pharmacy-interface/orders/{order_id}/approve-order")
    Observable<JsonObject> approveOrder(@Path("order_id") String orderId);

    @PUT("api/pharmacy/pharmacy-interface/orders/{order_id}/reject-order")
    Observable<JsonObject> rejectOrder(@Path("order_id") String orderId,@Query("comment") String comment);

    @PUT("api/pharmacy/pharmacy-interface/orders/{order_id}/update-status")
    Observable<JsonObject> updateOrderStatus(@Path("order_id") String orderId, @Query("status") String status, @Query("comment") String comment);


    @POST("api/pharmacy/pharmacy-interface/changepassword")
    Observable<JsonObject> changePassword(@Body HashMap<String, String> dto);

    @POST("api/support/addSupport")
    Observable<JsonObject> addSupportMessage(@Body HashMap<String, String> dto);

    @POST("api/pharmacy/pharmacy-interface/forgetpassword")
    Observable<JsonObject> changeforgetPassword(@Body HashMap<String, String> dto);

    @FormUrlEncoded
    @POST("oauth/token")
    Observable<JsonObject> login(
            @Field("username") String username
            , @Field("password") String password
            , @Field("grant_type") String grant_type
            , @Field("client_secret") String client_secret
            , @Field("client_id") String client_id
            , @Header("Authorization") String Basic
    );

    @POST("api/user/saveuseridentification")
    Observable<JsonObject> saveUserToken(@Body HashMap<String,String> dto);

    @GET("api/pharmacy/pharmacy-interface/pwverificationcode")
    Observable<JsonObject> generateOtpCode(@Query("mail") String email);

    @GET("api/pharmacy/pharmacy-interface/{userID}/accountDetails")
    Observable<UserModel> getAccountDetails(@Path("userID") String userID);

    @PUT("api/pharmacy/pharmacy-interface/branches/{id}/accounts/update")
    Observable<UserModel> updateAccountDetails(@Path("id") String userID, @Body UserModel userDTO);


}

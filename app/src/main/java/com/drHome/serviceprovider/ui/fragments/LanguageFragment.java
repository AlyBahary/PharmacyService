package com.drHome.serviceprovider.ui.fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.drHome.serviceprovider.R;
import com.drHome.serviceprovider.ui.activities.MoreActivity;
import com.drHome.serviceprovider.ui.activities.SplashActivity;
import com.drHome.serviceprovider.ui.helper.BasicFragment;
import com.drHome.serviceprovider.utils.AndroidApplication;
import com.drHome.serviceprovider.utils.CommonUtil;
import com.drHome.serviceprovider.utils.Config;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LanguageFragment extends BasicFragment {
    @BindView(R.id.toolbar_title_img)
    ImageView toolbarTitleImg;

    @BindView(R.id.toolbar_title_tv)
    TextView toolbarTitleTv;

    @BindView(R.id.english)
    RadioButton english;

    @BindView(R.id.arabic)
    RadioButton arabic;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_language, container, false);
        ButterKnife.bind(this,view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        toolbarTitleImg.setOnClickListener(v -> {
            ((MoreActivity) getActivity()).openMore();

        });
        toolbarTitleTv.setText(getText(R.string.Change_language));

        if (CommonUtil.getDataFromSharedPref("appLanguage").equals("ar")){
            arabic.setChecked(true);
            english.setChecked(false);
        }
        arabic.setOnClickListener(v -> {

            AndroidApplication.setAppLocale("ar");
            getActivity().finishAffinity();
            startActivity(new Intent(getContext(), SplashActivity.class));
        });
        english.setOnClickListener(v -> {
            AndroidApplication.setAppLocale("en");
            getActivity().finishAffinity();
            startActivity(new Intent(getContext(), SplashActivity.class));
        });

    }
}
package com.drHome.serviceprovider.ui.fragments.main;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.drHome.serviceprovider.R;
import com.drHome.serviceprovider.ui.activities.MoreActivity;
import com.drHome.serviceprovider.ui.activities.SplashActivity;
import com.drHome.serviceprovider.ui.helper.BasicFragment;
import com.drHome.serviceprovider.utils.AndroidApplication;
import com.drHome.serviceprovider.utils.CommonUtil;
import com.drHome.serviceprovider.utils.Config;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MoreFragment extends BasicFragment {

    private MoreViewModel mViewModel;

    @BindView(R.id.imageView)
    ImageView backImg;

    @BindView(R.id.name_tv)
    TextView nameTv;

    @BindView(R.id.profile_tv)
    TextView profileTv;

    @BindView(R.id.order_history_tv)
    TextView orderHistoryTv;

    @BindView(R.id.change_password_tv)
    TextView changePasswordTv;

    @BindView(R.id.about_us_tv)
    TextView aboutUsTv;

    @BindView(R.id.contact_us_tv)
    TextView contactUsTv;

    @BindView(R.id.change_lang_tv)
    TextView changeLangTv;

    @BindView(R.id.version_TV)
    TextView versionTv;

    @BindView(R.id.logout_tv)
    TextView logoutTv;

    public static MoreFragment newInstance() {
        return new MoreFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.more_fragment, container, false);
        ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
//        mViewModel = new ViewModelProvider(this).get(MoreViewModel.class);
        // TODO: Use the ViewModel

        nameTv.setText(CommonUtil.getDataFromSharedPref(Config.USER_NAME));
        backImg.setOnClickListener(v -> {
            getActivity().onBackPressed();
        });

        profileTv.setOnClickListener(v -> {
            ((MoreActivity)getActivity()).openPersonalInfo();
        });

        orderHistoryTv.setOnClickListener(v -> {
            ((MoreActivity)getActivity()).openOrderHistory();
        });

        changePasswordTv.setOnClickListener(v -> {
            ((MoreActivity)getActivity()).openChangePassword();
        });

        aboutUsTv.setOnClickListener(v -> {
            ((MoreActivity)getActivity()).openAboutUs();
        });

        changeLangTv.setOnClickListener(v -> {
            ((MoreActivity)getActivity()).openLanguage();
        });

        contactUsTv.setOnClickListener(v -> {
            ((MoreActivity)getActivity()).openContactUs();
        });

        logoutTv.setOnClickListener(v -> {
            CommonUtil.commonSharedPref().edit().clear();
            CommonUtil.commonSharedPref().edit().commit();
            CommonUtil.commonSharedPref().edit().remove(Config.BRANCH_ID).commit();
            getActivity().finishAffinity();
            startActivity(new Intent(getContext(), SplashActivity.class));
        });

        versionTv.setText("Doctor @ home v 0.0.5");
    }

}
package com.drHome.serviceprovider.ui.activities;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.lifecycle.ViewModelProvider;

import com.drHome.serviceprovider.R;
import com.drHome.serviceprovider.ui.helper.BasicActivity;
import com.drHome.serviceprovider.utils.Config;
import com.drHome.serviceprovider.viewmodel.AuthViewModel;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ResetPasswordActivity extends BasicActivity {
    @BindView(R.id.toolbar_title_img)
    ImageView toolbarTitleImg;

    @BindView(R.id.toolbar_title_tv)
    TextView toolbarTitleTv;

    @BindView(R.id.passwordEt)
    EditText passwordEt;

    @BindView(R.id.rePasswordEt)
    EditText rePasswordEt;

    @BindView(R.id.forget_password_button)
    Button forget_password_button;


    AuthViewModel authViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        ButterKnife.bind(this);

        authViewModel = new ViewModelProvider(this).get(AuthViewModel.class);


        toolbarTitleImg.setOnClickListener(v -> {
            finish();
        });
        toolbarTitleTv.setText(getText(R.string.Forget_password));

        forget_password_button.setOnClickListener(v -> {
            if (passwordEt.getText().toString().trim().length() < 7) {
                passwordEt.setError(getString(R.string.password_must_be_more_than_7_digits));
                passwordEt.requestFocus();
                return;
            }
            if (passwordEt.getText().toString().equals(rePasswordEt.getText().toString())){
                HashMap<String,String> map=new HashMap<>();
                map.put("email",getIntent().getStringExtra(Config.EMAIL));
                map.put("newPassword",passwordEt.getText().toString());
                map.put("pwCode",getIntent().getStringExtra(Config.CODE));
                authViewModel.changeforgetPassword(map);
                addProgressBar();
                authViewModel.getGenerateCodeMutableLiveData().observe(this,jsonObject -> {
                   hideProgressBar();
                    if (jsonObject.has("rspStatus")&&jsonObject.get("rspStatus").getAsString().equals("Success")){
                        addSnackbar(getString(R.string.password_change_successfully));
                    }else {
                        addSnackbar(jsonObject.get("message").getAsString());

                    }
                });
            }else {
                rePasswordEt.setError(getString(R.string.rePassword_is_not_matching_newPassword));
                rePasswordEt.requestFocus();
            }

        });
    }
}
package com.drHome.serviceprovider.ui.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.drHome.serviceprovider.R;
import com.drHome.serviceprovider.ui.helper.BasicActivity;
import com.drHome.serviceprovider.utils.CommonUtil;
import com.drHome.serviceprovider.utils.Config;
import com.nabinbhandari.android.permissions.PermissionHandler;
import com.nabinbhandari.android.permissions.Permissions;

import java.util.ArrayList;

import timber.log.Timber;

public class SplashActivity extends BasicActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        setSplashAnimation();

    }

    void setSplashAnimation() {
        Animation fadeInAnim = AnimationUtils.loadAnimation(this, R.anim.fadein);
        findViewById(R.id.logo).startAnimation(fadeInAnim);
        fadeInAnim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                Timber.e("Done.." + CommonUtil.getDataFromSharedPref(Config.BRANCH_ID));
                requestPermission();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    private void requestPermission() {
        String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.CALL_PHONE};
        String rationale = getString(R.string.please_provide_some_permissions);
        Permissions.Options options = new Permissions.Options()
                .setRationaleDialogTitle("")
                .setSettingsDialogTitle(getString(R.string.Important));
        Permissions.check(SplashActivity.this, permissions, rationale, options, new PermissionHandler() {
            @Override
            public void onGranted() {
                if (CommonUtil.getDataFromSharedPref(Config.BRANCH_ID).isEmpty())
                    startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                else
                    startActivity(new Intent(SplashActivity.this, MainActivity.class));

                finish();

            }

            @Override
            public void onDenied(Context context, ArrayList<String> deniedPermissions) {
                super.onDenied(context, deniedPermissions);
                requestPermission();
            }
        });

    }

}
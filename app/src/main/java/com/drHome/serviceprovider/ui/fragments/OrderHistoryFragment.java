package com.drHome.serviceprovider.ui.fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.drHome.serviceprovider.R;
import com.drHome.serviceprovider.adapters.OrderHistoryAdapter;
import com.drHome.serviceprovider.ui.activities.MoreActivity;
import com.drHome.serviceprovider.ui.activities.OrderDetailsActivity;
import com.drHome.serviceprovider.ui.helper.BasicFragment;
import com.drHome.serviceprovider.ui.helper.EndlessRecyclerViewScrollListener;
import com.drHome.serviceprovider.utils.CommonUtil;
import com.drHome.serviceprovider.utils.Config;
import com.drHome.serviceprovider.viewmodel.HomeViewModel;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

public class OrderHistoryFragment extends BasicFragment {
    private int previousTotal = 0;
    private boolean loading = true;
    private int visibleThreshold = 5;
    int firstVisibleItem, visibleItemCount, totalItemCount;

    @BindView(R.id.searchEt)
    EditText searchEt;

    @BindView(R.id.back_ic)
    ImageView back_ic;

    @BindView(R.id.all_tv)
    TextView allTv;

    @BindView(R.id.complete_tv)
    TextView complete_tv;

    @BindView(R.id.approve_tv)
    TextView approve_tv;

    @BindView(R.id.delivery_man_tv)
    TextView delivery_man_tv;

    @BindView(R.id.cancel_tv)
    TextView cancel_tv;

    @BindView(R.id.reject_tv)
    TextView reject_tv;

    @BindView(R.id.order_rv)
    RecyclerView orderRv;
    OrderHistoryAdapter orderHistoryAdapter;


    HomeViewModel homeViewModel;

    int currentPage = 0, lastPage = 0;
    String currentStatus = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_order_history, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        homeViewModel = new ViewModelProvider(this).get(HomeViewModel.class);
        back_ic.setOnClickListener(v -> {
            ((MoreActivity) getActivity()).openMore();
        });


        orderHistoryAdapter = new OrderHistoryAdapter(requireContext(), position -> {
            Log.d("TAG", "onViewCreated: clicked");
            startActivity(new Intent(getContext(), OrderDetailsActivity.class)
                    .putExtra(Config.ID, orderHistoryAdapter.getNewOrderModels().get(position).getId() + ""));

        });
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        orderRv.setLayoutManager(linearLayoutManager);

        orderRv.addOnScrollListener(new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
//
                Log.d("TAG", "onLoadMore: " + page);
                Log.d("TAG", "onLoadMore: " + totalItemsCount);

                if (page < (lastPage)) {
                    page++;
                    getOrders(currentStatus);
                    Timber.i("currentPage : " + " " + Integer.toString(page));
                }

            }


        });

        orderRv.setAdapter(orderHistoryAdapter);

        getOrders("");
        allTv.setOnClickListener(v -> {
            currentPage = 0;
            getOrders("");
            setColors(allTv);
        });
        complete_tv.setOnClickListener(v -> {
            currentPage = 0;
            getOrders("completed");
            setColors(complete_tv);

        });
        delivery_man_tv.setOnClickListener(v -> {
            currentPage = 0;
            getOrders("deliveryMan");
            setColors(delivery_man_tv);

        });
        approve_tv.setOnClickListener(v -> {
            currentPage = 0;
            getOrders("approved");
            setColors(approve_tv);

        });
        reject_tv.setOnClickListener(v -> {
            currentPage = 0;
            getOrders("rejected");
            setColors(reject_tv);

        });
        cancel_tv.setOnClickListener(v -> {
            currentPage = 0;
            getOrders("canceled");
            setColors(cancel_tv);

        });

        searchEt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    search(searchEt.getText().toString());
                    return true;
                }
                return false;
            }
        });

        if (!CommonUtil.getDataFromSharedPref(Config.USER_TYPE).equals("branchUser")) {
            allTv.setVisibility(View.GONE);
            approve_tv.setVisibility(View.GONE);
            reject_tv.setVisibility(View.GONE);
            cancel_tv.setVisibility(View.GONE);
            delivery_man_tv.performClick();
        }
    }

    private void getOrders(String status) {
        currentStatus = status;

        if (currentPage == 0)
            ((MoreActivity) getActivity()).addProgressBar();
        HashMap<String, String> map = new HashMap<>();
        map.put("branchId", CommonUtil.getDataFromSharedPref(Config.BRANCH_ID));
        if (!status.isEmpty())
            map.put("status", status);
        homeViewModel.getOrders(map, currentPage + "");
        homeViewModel.getOrderModelMutableLiveData().observe(getViewLifecycleOwner(), newOrderModel -> {
            ((MoreActivity) getActivity()).hideProgressBar();
            lastPage = newOrderModel.getTotalPages();
            if (currentPage == 0)
                orderHistoryAdapter.setNewOrderModels(newOrderModel.getOrderModel());
            else
                orderHistoryAdapter.addToNewOrderModels(newOrderModel.getOrderModel());
            currentPage++;
        });
    }

    private void setColors(TextView current) {
        allTv.setTextColor(getResources().getColor(R.color.text_color));
        allTv.setBackgroundResource(R.drawable.grey_rounded_bg2);

        complete_tv.setTextColor(getResources().getColor(R.color.text_color));
        complete_tv.setBackgroundResource(R.drawable.grey_rounded_bg2);

        approve_tv.setTextColor(getResources().getColor(R.color.text_color));
        approve_tv.setBackgroundResource(R.drawable.grey_rounded_bg2);

        delivery_man_tv.setTextColor(getResources().getColor(R.color.text_color));
        delivery_man_tv.setBackgroundResource(R.drawable.grey_rounded_bg2);

        cancel_tv.setTextColor(getResources().getColor(R.color.text_color));
        cancel_tv.setBackgroundResource(R.drawable.grey_rounded_bg2);

        reject_tv.setTextColor(getResources().getColor(R.color.text_color));
        reject_tv.setBackgroundResource(R.drawable.grey_rounded_bg2);


        current.setTextColor(getResources().getColor(R.color.white));
        current.setBackgroundResource(R.drawable.green_rounded_bg);

    }

    private void search(String id) {
        orderHistoryAdapter.searchByID(Integer.valueOf(id));

    }
}
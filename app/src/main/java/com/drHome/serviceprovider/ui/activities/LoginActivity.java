package com.drHome.serviceprovider.ui.activities;

import androidx.lifecycle.ViewModelProvider;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.text.InputType;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.auth0.android.jwt.JWT;
import com.drHome.serviceprovider.R;
import com.drHome.serviceprovider.ui.helper.BasicActivity;
import com.drHome.serviceprovider.utils.CommonUtil;
import com.drHome.serviceprovider.utils.Config;
import com.drHome.serviceprovider.viewmodel.AuthViewModel;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginActivity extends BasicActivity {

    @BindView(R.id.email_et)
    EditText emailEt;

    @BindView(R.id.passwordEt)
    EditText passwordEt;

    @BindView(R.id.eye_ic)
    ImageView eye_ic;

    @BindView(R.id.forget_password_tv)
    TextView forgetPasswordTv;
    @BindView(R.id.loginButton)
    TextView loginButton;

    AuthViewModel authViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        authViewModel = new ViewModelProvider(this).get(AuthViewModel.class);
        forgetPasswordTv.setPaintFlags(forgetPasswordTv.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        forgetPasswordTv.setOnClickListener(v -> {
            startActivity(new Intent(this, ForgetPasswordActivity.class));
        });

        eye_ic.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                int action = event.getActionMasked();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        passwordEt.setInputType(InputType.TYPE_CLASS_TEXT);
                        passwordEt.setSelection(passwordEt.getText().length());
                        return true;
                    case MotionEvent.ACTION_UP:
                    case MotionEvent.ACTION_OUTSIDE:
                        passwordEt.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                        passwordEt.setSelection(passwordEt.getText().length());
                }

                return false;
            }
        });
        loginButton.setOnClickListener(v -> {
            if (isEditTextEmpty(emailEt)) {
                setEditTextError(emailEt, getString(R.string.this_field_is_required));
                return;
            }
            if (isEditTextEmpty(passwordEt)) {
                setEditTextError(passwordEt, getString(R.string.this_field_is_required));
                return;
            }
            login();
        });

    }

    private void login() {
        addProgressBar();
//        ahmedsamy222
        authViewModel.login("BRANCH_USER/"+emailEt.getText().toString(), passwordEt.getText().toString());
        authViewModel.getLoginMutableLiveData().observe(this, jsonObject -> {
            hideProgressBar();
            if (jsonObject!=null){
                if (jsonObject.has("error")){
                    addSnackbar(jsonObject.get("error_description").getAsString());
                    return;
                }
                JWT jwt = new JWT(jsonObject.get("access_token").getAsString());
                CommonUtil.addToSharedPref(Config.ACCESS_TOKEN,"Bearer "+jsonObject.get("access_token").getAsString()+"");
                CommonUtil.addToSharedPref(Config.BRANCH_ID,jwt.getClaim("branchId").asInt()+"");
                CommonUtil.addToSharedPref(Config.USER_NAME,jwt.getClaim("user_name").asString()+"");
                CommonUtil.addToSharedPref(Config.USER_ID,jwt.getClaim("userId").asInt()+"");
                CommonUtil.addToSharedPref(Config.USER_TYPE,jwt.getClaim("branchUserType").asString()+"");
                finish();
                startActivity(new Intent(this, MainActivity.class));
            }
        });
        authViewModel.getLoginErrorMutableLiveData().observe(this, errorJsonObject -> {
            hideProgressBar();
            Toast.makeText(this, errorJsonObject.get("error").getAsString(), Toast.LENGTH_SHORT).show();
        });
    }
}
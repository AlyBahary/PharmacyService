package com.drHome.serviceprovider.ui.helper;

import android.app.Activity;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;

import androidx.fragment.app.Fragment;

import com.drHome.serviceprovider.utils.AndroidApplication;
import com.drHome.serviceprovider.utils.CommonUtil;

import java.util.Locale;

import dagger.hilt.android.AndroidEntryPoint;

@AndroidEntryPoint
public class BasicFragment extends Fragment {
    @Override
    public void onStart() {
        super.onStart();
      //  setLocale(this.getActivity(), CommonUtil.getCurrentLang());

    }

    @Override
    public void onResume() {
        super.onResume();
        //setLocale(this.getActivity(), CommonUtil.getCurrentLang());

    }

    public void setLocale(Activity activity, String languageCode) {
        Locale locale = new Locale(languageCode);
        Locale.setDefault(locale);
        Resources resources = activity.getResources();
        Configuration config = resources.getConfiguration();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            config.setLocale(locale);
        }
        if (Build.VERSION.SDK_INT >= 25) {
            getContext().createConfigurationContext(config);
            AndroidApplication.getAppContext().createConfigurationContext(config);
        }

        resources.updateConfiguration(config, resources.getDisplayMetrics());
    }

}

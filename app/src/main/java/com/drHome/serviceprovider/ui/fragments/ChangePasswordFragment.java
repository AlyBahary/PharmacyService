package com.drHome.serviceprovider.ui.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.drHome.serviceprovider.R;
import com.drHome.serviceprovider.ui.activities.MoreActivity;
import com.drHome.serviceprovider.ui.helper.BasicFragment;
import com.drHome.serviceprovider.utils.CommonUtil;
import com.drHome.serviceprovider.utils.Config;
import com.drHome.serviceprovider.viewmodel.AuthViewModel;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChangePasswordFragment extends BasicFragment {
    @BindView(R.id.toolbar_title_img)
    ImageView toolbarTitleImg;

    @BindView(R.id.toolbar_title_tv)
    TextView toolbarTitleTv;

    @BindView(R.id.oldPasswordEt)
    EditText oldPasswordEt;

    @BindView(R.id.newPasswordEt)
    EditText newPasswordEt;

    @BindView(R.id.reNewPasswordEt)
    EditText reNewPasswordEt;

    AuthViewModel authViewModel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_change_password, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        authViewModel = new ViewModelProvider(this).get(AuthViewModel.class);
        toolbarTitleImg.setOnClickListener(v -> {
            ((MoreActivity) getActivity()).openMore();

        });
        toolbarTitleTv.setText(getText(R.string.Change_Password));
        view.findViewById(R.id.change_password_btn).setOnClickListener(v -> {
            if (newPasswordEt.getText().toString().trim().length() < 7) {
                newPasswordEt.setError(getString(R.string.password_must_be_more_than_7_digits));
                return;
            }
            if (newPasswordEt.getText().toString().equals(reNewPasswordEt.getText().toString())) {
                changePassword();
            } else {
                reNewPasswordEt.setError(getString(R.string.rePassword_is_not_matching_newPassword));
            }
        });

    }

    private void changePassword() {
        ((MoreActivity) getActivity()).addProgressBar();
        HashMap<String, String> map = new HashMap<>();
        map.put("newPassword", newPasswordEt.getText().toString());
        map.put("oldPassword", oldPasswordEt.getText().toString());
        map.put("userId", CommonUtil.getDataFromSharedPref(Config.USER_ID));
        authViewModel.changePassword(map);
        authViewModel.getChangePasswordMutableLiveData().observe(getViewLifecycleOwner(), jsonObject -> {
            ((MoreActivity) getActivity()).hideProgressBar();
            if (jsonObject.has("message")) {
                ((MoreActivity) getActivity()).addSnackbar(jsonObject.get("message").getAsString());
            } else {
                ((MoreActivity) getActivity()).addShortToast(getString(R.string.password_change_successfully));
            }
        });
    }
}
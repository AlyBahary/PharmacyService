package com.drHome.serviceprovider.ui.activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.drHome.serviceprovider.R;
import com.drHome.serviceprovider.adapters.ServicesAdapter;
import com.drHome.serviceprovider.ui.helper.BasicActivity;
import com.drHome.serviceprovider.utils.Config;
import com.drHome.serviceprovider.viewmodel.HomeViewModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OrderDetailsActivity extends BasicActivity {

    @BindView(R.id.toolbar_title_img)
    ImageView toolbarTitleImg;

    @BindView(R.id.download_tv)
    TextView downloadTv;

    @BindView(R.id.accept_reject_view)
    LinearLayout acceptRejectView;

    @BindView(R.id.accep_btn)
    Button acceptBtn;

    @BindView(R.id.reject_btn)
    Button rejectBtn;
    //
    @BindView(R.id.status_view)
    ConstraintLayout statusView;


    //

    @BindView(R.id.avatar_img)
    ImageView avatarImg;

    @BindView(R.id.name_tv)
    TextView nameTv;

    @BindView(R.id.phone_tv)
    TextView phoneTv;

    @BindView(R.id.address_tv)
    TextView addressTv;

    @BindView(R.id.call_ic)
    ImageView callIc;


    @BindView(R.id.share_ic)
    ImageView shareIc;

    @BindView(R.id.Diagnose_tv)
    TextView DiagnoseTv;

    @BindView(R.id.type_tv)
    TextView typeTv;

    @BindView(R.id.complain_tv)
    TextView complainTv;


    @BindView(R.id.statusTv)
    TextView statusTv;

    @BindView(R.id.statusSpinner)
    Spinner statusSpinner;

    @BindView(R.id.applyStatusBtn)
    Button applyStatusBtn;


    //
    @BindView(R.id.service_rv)
    RecyclerView serviceRv;
    ServicesAdapter servicesAdapter;

    HomeViewModel homeViewModel;

    String status = "approved";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);
        ButterKnife.bind(this);
        addProgressBar();
        toolbarTitleImg.setOnClickListener(v -> {
            finish();
        });
        homeViewModel = new ViewModelProvider(this).get(HomeViewModel.class);
        homeViewModel.getOrderDetails(getIntent().getStringExtra(Config.ID));
        Log.d("TAG", "onCreate: 2");
        homeViewModel.getOrderDatailsResponseModelMutableLiveData().observe(this, orderDatailsResponseModel -> {
            hideProgressBar();
            if (orderDatailsResponseModel.getStatus().equals("New")) {
                statusView.setVisibility(View.GONE);
                acceptRejectView.setVisibility(View.VISIBLE);
            } else {
                statusView.setVisibility(View.VISIBLE);
                acceptRejectView.setVisibility(View.GONE);
            }
            setSpinner(orderDatailsResponseModel.getStatus().toLowerCase());
            statusTv.setText(orderDatailsResponseModel.getStatus());
            nameTv.setText(orderDatailsResponseModel.getUserData().getFullName());

            if (orderDatailsResponseModel.getUserData().getProfilePath() != null) {
                Glide.with(this).load(Config.BaseUrl + orderDatailsResponseModel.getUserData().getProfilePath())
                        .apply(RequestOptions.circleCropTransform().placeholder(R.drawable.avatar))
                        .into(avatarImg);

            }


            phoneTv.setText(orderDatailsResponseModel.getUserData().getMobileNumber());
            addressTv.setText(orderDatailsResponseModel.getAddress());
            typeTv.setText(getString(R.string.Type) + "\n" + orderDatailsResponseModel.getType());
            DiagnoseTv.setText(getString(R.string.Diagnose) + "\n" + orderDatailsResponseModel.getDiagnose());
            complainTv.setText(getString(R.string.Complain) + "\n" + orderDatailsResponseModel.getComplain());
            servicesAdapter.setOrderMedicineModels(orderDatailsResponseModel.getOrderMedicines());
            if (orderDatailsResponseModel.getType().equals("quickSell")) {
                addressTv.setTextColor(getResources().getColor(R.color.text_color_light2));
                shareIc.setVisibility(View.INVISIBLE);

            }

            addressTv.setOnClickListener(v -> {
                if (orderDatailsResponseModel.getType().equals("quickSell")) {
                    return;
                }
                String uri = String.format(Locale.ENGLISH, "geo:%f,%f", orderDatailsResponseModel.getLatitude(), orderDatailsResponseModel.getLongitude());
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                startActivity(intent);
            });
            callIc.setOnClickListener(v -> {

                Intent intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse(("tel:" + Uri.parse(orderDatailsResponseModel.getUserData().getMobileNumber()))));
                startActivity(intent);
            });
            shareIc.setOnClickListener(v -> {
                String uri = "http://maps.google.com/maps?q=" + orderDatailsResponseModel.getLatitude() + "," + orderDatailsResponseModel.getLongitude() + "&iwloc=A";

                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                String ShareSub = getString(R.string.Patient_Location);
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, ShareSub);
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, uri);
                startActivity(Intent.createChooser(sharingIntent, "Share via"));
            });
            downloadTv.setOnClickListener(v -> {
                if (orderDatailsResponseModel.getAttachment() != null) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(Config.BaseUrl + orderDatailsResponseModel.getAttachment())));

                }
            });

        });
        downloadTv.setPaintFlags(downloadTv.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        servicesAdapter = new ServicesAdapter(this, position -> {

        });
        serviceRv.setAdapter(servicesAdapter);


        acceptBtn.setOnClickListener(v -> {
            addProgressBar();
            homeViewModel.approveRequest(getIntent().getStringExtra(Config.ID));
            homeViewModel.getOrderStatusMutableLiveData().observe(this, jsonObject -> {
                hideProgressBar();
                setSpinner("approved");
                animatedTransaction();
            });

        });
        rejectBtn.setOnClickListener(v -> {

            Dialog dialog = new Dialog(this);
            dialog.setContentView(R.layout.reject_reason_dialoge);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.show();
            EditText noteEt = dialog.findViewById(R.id.note_et);
            dialog.findViewById(R.id.done_btn).setOnClickListener(v1 -> {
                if (!noteEt.getText().toString().trim().isEmpty()) {
                    dialog.dismiss();
                    addProgressBar();
                    homeViewModel.rejectRequest(getIntent().getStringExtra(Config.ID), noteEt.getText().toString());
                    homeViewModel.getOrderStatusMutableLiveData().observe(this, jsonObject -> {
                        hideProgressBar();
                        setSpinner("rejected");
                        finish();
                    });

                } else {
                    noteEt.setError(getString(R.string.this_field_is_required));
                    noteEt.requestFocus();
                }
            });


        });
    }

    private void animatedTransaction() {
        acceptRejectView.setVisibility(View.VISIBLE);
        statusView.setVisibility(View.VISIBLE);

        Animation fadeInAnim = AnimationUtils.loadAnimation(this, R.anim.fadein_faster);
        Animation fadeOutAnim = AnimationUtils.loadAnimation(this, R.anim.fade_out_faster);

        acceptRejectView.startAnimation(fadeOutAnim);
        statusView.startAnimation(fadeInAnim);
        fadeInAnim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                acceptRejectView.setVisibility(View.GONE);
                statusView.setVisibility(View.VISIBLE);

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    private void setSpinner(String currentStatus) {
        ArrayList<String> statusList = new ArrayList<>();

        switch (currentStatus) {
            case "completed":
            case "cancelled":
            case "rejected":
            case "returned":
                Log.d("status", "setSpinner: completed");
                acceptRejectView.setVisibility(View.GONE);
                statusView.setVisibility(View.GONE);
                break;
            case "paid":
                statusList.add("approved");
                statusList.add("rejected");
                break;
            case "approved":
                statusList.add("deliveryMan");
                statusList.add("completed");
                break;
            case "received":
            case "delivery man":
                statusList.add("completed");
                break;


        }
//
//        statusList.add("approved");
//        statusList.add("rejected");
//        // statusList.add("New");
//        statusList.add("completed");
//        statusList.add("cancelled");
//        statusList.add("deliveryMan");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, statusList) {
            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View v = super.getView(position, convertView, parent);

                ((TextView) v).setTextColor(
                        getResources().getColorStateList(R.color.colorPrimary)
                );
                return v;
            }
        };

        statusSpinner.setAdapter(dataAdapter);

        statusSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                status = statusList.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        applyStatusBtn.setOnClickListener(v -> {
            addProgressBar();
            if (status.equals("rejected")) {
                Dialog dialog = new Dialog(this);
                dialog.setContentView(R.layout.reject_reason_dialoge);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
                EditText noteEt = dialog.findViewById(R.id.note_et);
                dialog.findViewById(R.id.done_btn).setOnClickListener(v1 -> {
                    if (!noteEt.getText().toString().trim().isEmpty()) {
                        dialog.dismiss();
                        addProgressBar();
                        homeViewModel.updateStatus(getIntent().getStringExtra(Config.ID), status, noteEt.getText().toString());
                        homeViewModel.getOrderStatusMutableLiveData().observe(OrderDetailsActivity.this, jsonObject -> {
                            setSpinner(status);
                            hideProgressBar();
                            statusTv.setText(status);
                            Toast.makeText(OrderDetailsActivity.this, "" + getString(R.string.Status_Updated_Successfully), Toast.LENGTH_SHORT).show();
                        });

                    } else {
                        noteEt.setError(getString(R.string.this_field_is_required));
                        noteEt.requestFocus();
                    }
                });


            } else {
                homeViewModel.updateStatus(getIntent().getStringExtra(Config.ID), status, "");
                homeViewModel.getOrderStatusMutableLiveData().observe(OrderDetailsActivity.this, jsonObject -> {
                    setSpinner(status);
                    hideProgressBar();
                    statusTv.setText(status);
                    Toast.makeText(OrderDetailsActivity.this, "" + getString(R.string.Status_Updated_Successfully), Toast.LENGTH_SHORT).show();
                });
            }
        });
    }

}
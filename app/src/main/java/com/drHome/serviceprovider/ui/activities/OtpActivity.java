package com.drHome.serviceprovider.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.lifecycle.ViewModelProvider;

import com.drHome.serviceprovider.R;
import com.drHome.serviceprovider.ui.helper.BasicActivity;
import com.drHome.serviceprovider.utils.Config;
import com.drHome.serviceprovider.viewmodel.AuthViewModel;
import com.poovam.pinedittextfield.PinField;
import com.poovam.pinedittextfield.SquarePinField;

import org.jetbrains.annotations.NotNull;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OtpActivity extends BasicActivity {
    @BindView(R.id.toolbar_title_img)
    ImageView toolbarTitleImg;

    @BindView(R.id.toolbar_title_tv)
    TextView toolbarTitleTv;

    @BindView(R.id.verify_btn)
    TextView verifyBtn;

    AuthViewModel authViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);

        ButterKnife.bind(this);

        toolbarTitleImg.setOnClickListener(v -> {
            finish();
        });
        toolbarTitleTv.setText(getText(R.string.Forget_password));


        SquarePinField linePinField = findViewById(R.id.squareField);
        linePinField.setOnTextCompleteListener(new PinField.OnTextCompleteListener() {
            @Override
            public boolean onTextComplete(@NotNull String enteredText) {
                if (enteredText.equals(getIntent().getStringExtra(Config.CODE))) {
                    startActivity(new Intent(OtpActivity.this, ResetPasswordActivity.class)
                    .putExtra(Config.CODE,enteredText)
                    .putExtra(Config.EMAIL,getIntent().getStringExtra(Config.EMAIL))
                    );
                }else {
                    addSnackbar(getString(R.string.not_vaild_code));
                }
                return true; // Return false to keep the keyboard open else return true to close the keyboard
            }
        });

//        verifyBtn.setOnClickListener(v -> {
//            startActivity(new Intent(this, ResetPasswordActivity.class));
//        });

    }
}
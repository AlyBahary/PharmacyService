package com.drHome.serviceprovider.ui.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.drHome.serviceprovider.R;
import com.drHome.serviceprovider.ui.activities.MoreActivity;
import com.drHome.serviceprovider.ui.helper.BasicFragment;
import com.drHome.serviceprovider.utils.CommonUtil;
import com.drHome.serviceprovider.utils.Config;
import com.drHome.serviceprovider.viewmodel.AuthViewModel;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ContactUsFragment extends BasicFragment {
    @BindView(R.id.toolbar_title_img)
    ImageView toolbarTitleImg;

    @BindView(R.id.toolbar_title_tv)
    TextView toolbarTitleTv;

    @BindView(R.id.nameEt)
    EditText nameEt;

    @BindView(R.id.emailEt)
    EditText emailEt;

    @BindView(R.id.messageEt)
    EditText messageEt;

    @BindView(R.id.send_button)
    Button send_button;

    AuthViewModel authViewModel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_contact_us, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        authViewModel = new ViewModelProvider(this).get(AuthViewModel.class);
        toolbarTitleImg.setOnClickListener(v -> {
            ((MoreActivity) getActivity()).openMore();
        });
        toolbarTitleTv.setText(getText(R.string.Contact_us));
        send_button.setOnClickListener(v -> {
            if (nameEt.getText().toString().trim().isEmpty()) {
                ((MoreActivity) getActivity()).setEditTextError(nameEt, getString(R.string.this_field_is_required));
                return;
            } else if (emailEt.getText().toString().trim().isEmpty()) {
                ((MoreActivity) getActivity()).setEditTextError(emailEt, getString(R.string.this_field_is_required));
                return;


            } else if (!CommonUtil.isValidEmail(emailEt.getText().toString().trim())) {
                ((MoreActivity) getActivity()).setEditTextError(emailEt, getString(R.string.this_mail_not_Valid));
                return;

            } else if (messageEt.getText().toString().trim().isEmpty()) {
                ((MoreActivity) getActivity()).setEditTextError(messageEt, getString(R.string.this_field_is_required));
                return;

            }
            addSupportMessage();
        });
    }

    private void addSupportMessage() {
        ((MoreActivity) getActivity()).addProgressBar();
        HashMap<String, String> dto = new HashMap<>();
        dto.put("message", messageEt.getText().toString());
        dto.put("subject", "Contact Us");
        dto.put("userId", CommonUtil.getDataFromSharedPref(Config.USER_ID));
        authViewModel.addSupportMessage(dto);
        authViewModel.getAddSupportMessageMutableLiveData().observe(getViewLifecycleOwner(), jsonObject -> {
            Log.d("TAG", "addSupportMessage: ");
            ((MoreActivity) getActivity()).hideProgressBar();
            ((MoreActivity) getActivity()).addLongToast(getString(R.string.Message_Sent_Successfully));
            toolbarTitleImg.performClick();
        });
        authViewModel.getAddSupportMessageErrorMutableLiveData().observe(getViewLifecycleOwner(), jsonObject -> {
            Log.d("TAG", "addSupportMessage: error");

            ((MoreActivity) getActivity()).hideProgressBar();
            ((MoreActivity) getActivity()).addSnackbar(jsonObject.get("message").getAsString());

        });
    }
}
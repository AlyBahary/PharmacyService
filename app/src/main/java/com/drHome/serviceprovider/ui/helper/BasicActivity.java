
package com.drHome.serviceprovider.ui.helper;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;


import com.drHome.serviceprovider.R;
import com.drHome.serviceprovider.utils.AndroidApplication;
import com.drHome.serviceprovider.utils.CommonUtil;
import com.drHome.serviceprovider.utils.Config;
import com.google.android.material.snackbar.Snackbar;

import java.util.Locale;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;
import androidx.core.view.ViewCompat;

import dagger.hilt.android.AndroidEntryPoint;

@AndroidEntryPoint
public class BasicActivity extends AppCompatActivity {
    private Locale mCurrentLocale;
    private RelativeLayout layout;
    View parentLayout;
    Snackbar snackbar;


    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    public void setLocale(Activity activity, String languageCode) {
        Locale locale = new Locale(languageCode);
        Locale.setDefault(locale);
        Resources resources = activity.getResources();
        Configuration config = resources.getConfiguration();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            config.setLocale(locale);
        }
        if (Build.VERSION.SDK_INT >= 25) {
            AndroidApplication.getAppContext().createConfigurationContext(config);
            this.createConfigurationContext(config);
        }
        resources.updateConfiguration(config, resources.getDisplayMetrics());
    }


    @Override
    protected void onStart() {
        super.onStart();
        setLocale(this,CommonUtil.getCurrentLang());
        parentLayout = findViewById(android.R.id.content);
        snackbar = Snackbar.make(parentLayout, "", Snackbar.LENGTH_LONG);

        updateBaseContextLocale(getApplicationContext());

    }

    @Override
    protected void onRestart() {
        super.onRestart();
       // languageCheck();
    }

    protected void languageCheck() {
        SharedPreferences sharedPref = getSharedPreferences(Config.PREFS_NAME, Context.MODE_PRIVATE);
        Locale locale = new Locale(sharedPref.getString("appLanguage", "en"));
        Locale.setDefault(locale);

        if (!locale.equals(mCurrentLocale)) {
            mCurrentLocale = locale;
            recreate();
            CommonUtil.makeDefaultLocaleToArabic(getBaseContext());
        }
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        CommonUtil.makeDefaultLocaleToArabic(getBaseContext());
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(updateBaseContextLocale(base));
    }

    private Context updateBaseContextLocale(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(Config.PREFS_NAME, Context.MODE_PRIVATE);
        String language = sharedPref.getString("appLanguage", "en"); // Helper method to get saved language from SharedPreferences
        Locale locale = new Locale(language);
        Locale.setDefault(locale);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return updateResourcesLocale(context, locale);
        }

        return updateResourcesLocaleLegacy(context, locale);
    }

    @TargetApi(Build.VERSION_CODES.N)
    private Context updateResourcesLocale(Context context, Locale locale) {
        Configuration configuration = context.getResources().getConfiguration();
        configuration.setLocale(locale);
        return context.createConfigurationContext(configuration);
    }

    @SuppressWarnings("deprecation")
    private Context updateResourcesLocaleLegacy(Context context, Locale locale) {
        Resources resources = context.getResources();
        Configuration configuration = resources.getConfiguration();
        configuration.locale = locale;
        resources.updateConfiguration(configuration, resources.getDisplayMetrics());
        return context;
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);

    }

    //get last state
    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        AndroidApplication.setAppLocale(CommonUtil.commonSharedPref().getString("appLanguage", "en"));
    }


    private void initView() {
        if (layout == null) {
            layout = new RelativeLayout(this);
            layout.setClickable(true);
            ProgressBar progressBar = new ProgressBar(this, null, android.R.attr.progressBarStyleLarge);
            progressBar.setIndeterminate(true);
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(100, 100);
            LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
            params.addRule(RelativeLayout.CENTER_IN_PARENT);
            layout.addView(progressBar, params);
            this.addContentView(layout, params1);
            layout.setBackgroundColor(ContextCompat.getColor(this, R.color.transparentDarkBlue));
            layout.setVisibility(View.VISIBLE);
        }
    }







    public RelativeLayout addProgressBar() {
        initView();
        layout.setVisibility(View.VISIBLE);
        return layout;
    }
    public void hideProgressBar() {
        layout.setVisibility(View.GONE);
    }
    public void addSnackbar(String text) {
        snackbar.setText(text);
        ViewCompat.setLayoutDirection(snackbar.getView(), ViewCompat.LAYOUT_DIRECTION_RTL);
        snackbar.setActionTextColor(getResources().getColor(android.R.color.holo_orange_dark))
                .show();

    }
    public Boolean isEditTextEmpty(EditText editText) {

        if (editText.getText().toString().trim().isEmpty()) {
            //   editText.setError("هذا الحقل مطلوب");
            //editText.requestFocus();
            return true;
        }
        return false;
    }
    public void setEditTextError(EditText editText, String s) {
        editText.setError(s);
        editText.requestFocus();
    }
    public void addLongToast(String message){
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }
    public void addShortToast(String message){
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

}

package com.drHome.serviceprovider.ui.activities;

import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.drHome.serviceprovider.R;
import com.drHome.serviceprovider.ui.helper.BasicActivity;
import com.drHome.serviceprovider.utils.Config;
import com.drHome.serviceprovider.viewmodel.AuthViewModel;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ForgetPasswordActivity extends BasicActivity {

    @BindView(R.id.forget_password_button)
    Button forgetPasswordButton;

    @BindView(R.id.toolbar_title_img)
    ImageView toolbarTitleImg;

    @BindView(R.id.toolbar_title_tv)
    TextView toolbarTitleTv;

    @BindView(R.id.email_et)
    EditText emailEt;

    AuthViewModel authViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);
        ButterKnife.bind(this);
        authViewModel = new ViewModelProvider(this).get(AuthViewModel.class);

        forgetPasswordButton.setOnClickListener(v -> {
            if (isEditTextEmpty(emailEt)) {
                setEditTextError(emailEt, getString(R.string.this_field_is_required));
                return;
            }
            authViewModel.generateOtpCode(emailEt.getText().toString());
            addProgressBar();
            authViewModel.getGenerateCodeMutableLiveData().observe(this, jsonObject -> {
                if (jsonObject.has("pwCode")) {
                    Log.d("TAG", "onCreate:CODE ");
                    hideProgressBar();
                    finish();
                    startActivity(new Intent(this, OtpActivity.class)
                            .putExtra(Config.CODE, jsonObject.get("pwCode").getAsString())
                            .putExtra(Config.EMAIL, emailEt.getText().toString())
                    );
                    Log.d("TAG", "onCreate:CODE 2");
                }
            });
            authViewModel.getGenerateCodeErrorMutableLiveData().observe(this, jsonObject -> {
                hideProgressBar();
                addSnackbar(getString(R.string.this_mail_not_found));
            });

        });
        toolbarTitleImg.setOnClickListener(v -> {
            finish();
        });
        toolbarTitleTv.setText(getText(R.string.Forget_password));

    }
}
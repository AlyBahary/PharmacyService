package com.drHome.serviceprovider.ui.activities;

import android.os.Bundle;

import com.drHome.serviceprovider.R;
import com.drHome.serviceprovider.ui.fragments.AboutUsFragment;
import com.drHome.serviceprovider.ui.fragments.ChangePasswordFragment;
import com.drHome.serviceprovider.ui.fragments.ContactUsFragment;
import com.drHome.serviceprovider.ui.fragments.LanguageFragment;
import com.drHome.serviceprovider.ui.fragments.OrderHistoryFragment;
import com.drHome.serviceprovider.ui.fragments.PersonalInfoFragment;
import com.drHome.serviceprovider.ui.fragments.main.MoreFragment;
import com.drHome.serviceprovider.ui.helper.BasicActivity;

public class MoreActivity extends BasicActivity {

    private Boolean isCurrentMore = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.more_activity);
        if (savedInstanceState == null) {
            openMore();
        }
    }

    public void openMore() {
        isCurrentMore = true;
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, new MoreFragment())
                .commitNow();
    }

    public void openPersonalInfo() {
        isCurrentMore = false;
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, new PersonalInfoFragment())
                .commitNow();
    }

    public void openOrderHistory() {
        isCurrentMore = false;
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, new OrderHistoryFragment())
                .commitNow();
    }

    public void openLanguage() {
        isCurrentMore = false;
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, new LanguageFragment())
                .commitNow();
    }

    public void openContactUs() {
        isCurrentMore = false;
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, new ContactUsFragment())
                .commitNow();
    }

    public void openChangePassword() {
        isCurrentMore = false;
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, new ChangePasswordFragment())
                .commitNow();
    }

    public void openAboutUs() {
        isCurrentMore = false;
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, new AboutUsFragment())
                .commitNow();
    }

    @Override
    public void onBackPressed() {
        if (isCurrentMore)
            super.onBackPressed();
        else
            openMore();
    }
}
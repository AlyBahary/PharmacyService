package com.drHome.serviceprovider.ui.activities;

import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.drHome.serviceprovider.R;
import com.drHome.serviceprovider.adapters.NewOrdersAdapter;
import com.drHome.serviceprovider.adapters.StatisticsAdapter;
import com.drHome.serviceprovider.ui.helper.BasicActivity;
import com.drHome.serviceprovider.utils.CommonUtil;
import com.drHome.serviceprovider.utils.Config;
import com.drHome.serviceprovider.viewmodel.HomeViewModel;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BasicActivity {

    HomeViewModel homeViewModel;

    @BindView(R.id.name_tv)
    TextView nameTv;

    @BindView(R.id.menu_img)
    ImageView menuImg;

    @BindView(R.id.avatar_img)
    ImageView avatarImg;

    @BindView(R.id.statics_tv)
    TextView staticsTv;

    @BindView(R.id.statics_rv)
    RecyclerView staticsRv;
    StatisticsAdapter statisticsAdapter;


    @BindView(R.id.order_rv)
    RecyclerView orderRv;
    NewOrdersAdapter newOrdersAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        homeViewModel = new ViewModelProvider(this).get(HomeViewModel.class);
        homeViewModel.saveUserToken();
        nameTv.setText(CommonUtil.getDataFromSharedPref(Config.USER_NAME));
        menuImg.setOnClickListener(v -> {
            startActivity(new Intent(this, MoreActivity.class));
        });

        avatarImg.setOnClickListener(v -> {
            startActivity(new Intent(this, MoreActivity.class));
        });

        newOrdersAdapter = new NewOrdersAdapter(this, position -> {
            startActivity(new Intent(this, OrderDetailsActivity.class)
                    .putExtra(Config.ID, newOrdersAdapter.getOrderModels().get(position).getId() + "")
            );
        });
        orderRv.setAdapter(newOrdersAdapter);


        statisticsAdapter = new StatisticsAdapter(this, position -> {

        });
        staticsRv.setAdapter(statisticsAdapter);
        GridLayoutManager mGridLayoutManager = new GridLayoutManager(this, CommonUtil.calculateNoOfColumns(this, (float) 200.0));
        staticsRv.setLayoutManager(mGridLayoutManager);

        //todo: incase delivery man
        if (!CommonUtil.getDataFromSharedPref(Config.USER_TYPE).equals("branchUser")) {
            staticsTv.setVisibility(View.GONE);
            staticsRv.setVisibility(View.GONE);
        } else
            getDashBoardData();

        SwipeRefreshLayout pullToRefresh = findViewById(R.id.pullToRefresh);
        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getDashBoardData();
                getNewOrders();
                pullToRefresh.setRefreshing(false);
            }
        });
    }

    private void getDashBoardData() {
        addProgressBar();
        homeViewModel.getDashBoardData(CommonUtil.getDataFromSharedPref(Config.BRANCH_ID));
        homeViewModel.getDashBoardDataModelMutableLiveData().observe(this, dashBoardDataModel -> {
            hideProgressBar();
            statisticsAdapter.setCancelled(dashBoardDataModel.getCanceledOrderCount() + "");
            statisticsAdapter.setCompleted(dashBoardDataModel.getCompletedOrderCount() + "");
            statisticsAdapter.setNewOrders(dashBoardDataModel.getNewOrderCount() + "");
            statisticsAdapter.setReturned(dashBoardDataModel.getReturnedOrderCount() + "");
            statisticsAdapter.notifyDataSetChanged();
        });
    }

    private void getNewOrders() {
        addProgressBar();
        HashMap<String, String> dto = new HashMap<>();
        dto.put("branchId", CommonUtil.getDataFromSharedPref(Config.BRANCH_ID));
        dto.put("pharmacyId", "1");
        if (!CommonUtil.getDataFromSharedPref(Config.USER_TYPE).equals("branchUser")) {
            dto.put("status", "deliveryMan");
        } else
            dto.put("status", "paid");
        homeViewModel.getNewOrders(dto, "0");
        homeViewModel.getNewOrderModelMutableLiveData().observe(this, newOrderModel -> {
            hideProgressBar();
            newOrdersAdapter.setOrderModels(newOrderModel.getOrderModel(), 0);
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        getNewOrders();
        getDashBoardData();
    }
}
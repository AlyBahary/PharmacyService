package com.drHome.serviceprovider.ui.fragments;

import android.app.DatePickerDialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.drHome.serviceprovider.R;
import com.drHome.serviceprovider.models.UserModel;
import com.drHome.serviceprovider.ui.activities.MoreActivity;
import com.drHome.serviceprovider.ui.helper.BasicFragment;
import com.drHome.serviceprovider.utils.CommonUtil;
import com.drHome.serviceprovider.utils.Config;
import com.drHome.serviceprovider.viewmodel.AuthViewModel;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PersonalInfoFragment extends BasicFragment implements DatePickerDialog.OnDateSetListener {

    @BindView(R.id.toolbar_title_img)
    ImageView toolbarTitleImg;

    @BindView(R.id.toolbar_title_tv)
    TextView toolbarTitleTv;


    @BindView(R.id.button)
    Button button;


    @BindView(R.id.f_nameEt)
    EditText firstNameEt;

    @BindView(R.id.l_nameEt)
    EditText LastNameEt;

    @BindView(R.id.emailEt)
    EditText emailEt;

    @BindView(R.id.phoneEt)
    EditText phoneEt;

    @BindView(R.id.date_et)
    EditText date_et;

    UserModel userModel;

    AuthViewModel authViewModel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_personal_info, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        authViewModel = new ViewModelProvider(this).get(AuthViewModel.class);
        authViewModel.getAccountDetails(CommonUtil.getDataFromSharedPref(Config.USER_ID));
        ((MoreActivity) getActivity()).addProgressBar();
        authViewModel.getUserProfileMutableLiveData().observe(getViewLifecycleOwner(), userModel -> {
            ((MoreActivity) getActivity()).hideProgressBar();
            this.userModel = userModel;
            firstNameEt.setText(userModel.getFirstName());
            LastNameEt.setText(userModel.getLastName());
            emailEt.setText(userModel.getEmail());
            phoneEt.setText(userModel.getPhoneNumber());
        });

        button.setOnClickListener(v -> {
            ((MoreActivity) getActivity()).addProgressBar();
            userModel.setFirstName(firstNameEt.getText().toString().trim().isEmpty() ? userModel.getFirstName() : firstNameEt.getText().toString());
            userModel.setLastName(LastNameEt.getText().toString().trim().isEmpty() ? userModel.getLastName() : LastNameEt.getText().toString());
            userModel.setEmail(emailEt.getText().toString().trim().isEmpty() ? userModel.getEmail() : emailEt.getText().toString());
            userModel.setPhoneNumber(phoneEt.getText().toString().trim().isEmpty() ? userModel.getPhoneNumber() : phoneEt.getText().toString());
            authViewModel.updateAccountDetails(CommonUtil.getDataFromSharedPref(Config.BRANCH_ID), userModel);
            authViewModel.getUserProfileMutableLiveData().observe(getViewLifecycleOwner(), userModel -> {
                ((MoreActivity) getActivity()).hideProgressBar();
                ((MoreActivity) getActivity()).addSnackbar(getString(R.string.profile_updated_successfully));

            });
        });

        date_et.setOnClickListener(v -> {
            new DatePickerDialog(requireContext(), R.style.DialogTheme, this, 1920, 0, 1).show();
        });


        toolbarTitleImg.setOnClickListener(v -> {
            ((MoreActivity) getActivity()).openMore();
        });
        toolbarTitleTv.setText(getText(R.string.Profile));

    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

        date_et.setText(dayOfMonth + "/" + (month + 1) + "/" + year);
    }
}
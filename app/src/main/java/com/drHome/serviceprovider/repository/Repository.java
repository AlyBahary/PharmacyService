package com.drHome.serviceprovider.repository;

import android.provider.Settings;
import android.util.Base64;

import com.drHome.serviceprovider.models.DashBoardDataModel;
import com.drHome.serviceprovider.models.NewOrderModel;
import com.drHome.serviceprovider.models.OrderDatailsResponseModel;
import com.drHome.serviceprovider.models.UserModel;
import com.drHome.serviceprovider.network.ApiService;
import com.drHome.serviceprovider.utils.AndroidApplication;
import com.drHome.serviceprovider.utils.CommonUtil;
import com.drHome.serviceprovider.utils.Config;
import com.google.gson.JsonObject;

import java.util.HashMap;

import javax.inject.Inject;

import io.reactivex.rxjava3.core.Observable;


public class Repository {
    private ApiService apiService;

    @Inject
    public Repository(ApiService apiService) {
        this.apiService = apiService;
    }


    public Observable<JsonObject> changePassword(HashMap<String, String> dto) {
        return apiService.changePassword(dto);
    }

    public Observable<JsonObject> addSupportMessage(HashMap<String, String> dto) {
        return apiService.addSupportMessage(dto);
    }

    public Observable<JsonObject> login(
            String username
            , String password
    ) {
        return apiService.login(username
                , password
                , "password"
                , "password"
                , "DoctorHome"
                , "Basic " + Base64.encodeToString("DoctorHome:password".getBytes(), Base64.NO_WRAP));
    }

    public Observable<JsonObject> saveUserToken(

    ) {
        HashMap hashMap = new HashMap();
        hashMap.put("deviceID", "" + Settings.Secure.getString(AndroidApplication.getAppContext().getContentResolver(),
                Settings.Secure.ANDROID_ID));
        hashMap.put("deviceName", "Android");
        hashMap.put("deviceOS", "Android");
        hashMap.put("id", ""+ CommonUtil.getDataFromSharedPref(Config.FIREBASE_TOKEN));
        hashMap.put("userId", ""+CommonUtil.getDataFromSharedPref(Config.USER_ID));
        hashMap.put("userLocale", "en");
        return apiService.saveUserToken(hashMap);
    }

    public Observable<JsonObject> generateOtpCode(String email) {
        return apiService.generateOtpCode(email);
    }


 public Observable<JsonObject> changeforgetPassword(HashMap<String,String> dto) {
        return apiService.changeforgetPassword(dto);
    }

    public Observable<UserModel> getAccountDetails(String userID) {
        return apiService.getAccountDetails(userID);
    }

    public Observable<UserModel> updateAccountDetails(String userID, UserModel userModel) {
        return apiService.updateAccountDetails(userID, userModel);
    }

    public Observable<DashBoardDataModel> getDashBoardData(String branchId) {
        return apiService.getDashBoardData(branchId);
    }

    public Observable<NewOrderModel> getNewOrders(HashMap<String, String> dto, String pageNumber) {
        return apiService.getNewOrders(dto, pageNumber);
    }

    public Observable<OrderDatailsResponseModel> getOrderDetails(String orderId) {
        return apiService.getOrderDetails(orderId);
    }

    public Observable<JsonObject> approveOrder(String orderId) {
        return apiService.approveOrder(orderId);
    }

    public Observable<JsonObject> rejectOrder(String orderId,String note) {
        return apiService.rejectOrder(orderId,note);
    }

    public Observable<JsonObject> updateOrderStatus(String orderId, String status,String comment) {
        return apiService.updateOrderStatus(orderId, status,comment);
    }
}

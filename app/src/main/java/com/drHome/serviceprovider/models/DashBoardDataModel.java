package com.drHome.serviceprovider.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DashBoardDataModel {
    @SerializedName("newOrderCount")
    @Expose
    private Integer newOrderCount;
    @SerializedName("canceledOrderCount")
    @Expose
    private Integer canceledOrderCount;
    @SerializedName("completedOrderCount")
    @Expose
    private Integer completedOrderCount;
    @SerializedName("returnedOrderCount")
    @Expose
    private Integer returnedOrderCount;

    public Integer getNewOrderCount() {
        return newOrderCount;
    }

    public void setNewOrderCount(Integer newOrderCount) {
        this.newOrderCount = newOrderCount;
    }

    public Integer getCanceledOrderCount() {
        return canceledOrderCount;
    }

    public void setCanceledOrderCount(Integer canceledOrderCount) {
        this.canceledOrderCount = canceledOrderCount;
    }

    public Integer getCompletedOrderCount() {
        return completedOrderCount;
    }

    public void setCompletedOrderCount(Integer completedOrderCount) {
        this.completedOrderCount = completedOrderCount;
    }

    public Integer getReturnedOrderCount() {
        return returnedOrderCount;
    }

    public void setReturnedOrderCount(Integer returnedOrderCount) {
        this.returnedOrderCount = returnedOrderCount;
    }
}

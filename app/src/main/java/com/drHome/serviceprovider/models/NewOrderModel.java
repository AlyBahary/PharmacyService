package com.drHome.serviceprovider.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class NewOrderModel {
    @SerializedName("content")
    @Expose
    private ArrayList<OrderModel> orderModel;
    @SerializedName("totalPages")
    @Expose
    private Integer totalPages;
    @SerializedName("totalElements")
    @Expose
    private Integer totalElements;

    public ArrayList<OrderModel> getOrderModel() {
        return orderModel;
    }

    public void setOrderModel(ArrayList<OrderModel> orderModel) {
        this.orderModel = orderModel;
    }

    public Integer getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(Integer totalPages) {
        this.totalPages = totalPages;
    }

    public Integer getTotalElements() {
        return totalElements;
    }

    public void setTotalElements(Integer totalElements) {
        this.totalElements = totalElements;
    }
}

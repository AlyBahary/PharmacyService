package com.drHome.serviceprovider.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderModel {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("orderNumber")
    @Expose
    private String orderNumber;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("isAccepted")
    @Expose
    private Boolean isAccepted;
    @SerializedName("isDeliveryMan")
    @Expose
    private Boolean isDeliveryMan;
    @SerializedName("isReceived")
    @Expose
    private Boolean isReceived;
    @SerializedName("userData")
    @Expose
    private UserDataModel userData;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Boolean getAccepted() {
        return isAccepted;
    }

    public void setAccepted(Boolean accepted) {
        isAccepted = accepted;
    }

    public Boolean getDeliveryMan() {
        return isDeliveryMan;
    }

    public void setDeliveryMan(Boolean deliveryMan) {
        isDeliveryMan = deliveryMan;
    }

    public Boolean getReceived() {
        return isReceived;
    }

    public void setReceived(Boolean received) {
        isReceived = received;
    }

    public UserDataModel getUserData() {
        return userData;
    }

    public void setUserData(UserDataModel userData) {
        this.userData = userData;
    }
}

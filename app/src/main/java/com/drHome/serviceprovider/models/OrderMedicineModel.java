package com.drHome.serviceprovider.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderMedicineModel {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("medicineName")
    @Expose
    private String medicineName;
    @SerializedName("quantity")
    @Expose
    private Integer quantity;
    @SerializedName("salePrice")
    @Expose
    private Integer salePrice;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMedicineName() {
        return medicineName;
    }

    public void setMedicineName(String medicineName) {
        this.medicineName = medicineName;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(Integer salePrice) {
        this.salePrice = salePrice;
    }
}

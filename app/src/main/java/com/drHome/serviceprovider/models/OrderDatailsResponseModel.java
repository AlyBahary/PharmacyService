package com.drHome.serviceprovider.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class OrderDatailsResponseModel {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("diagnose")
    @Expose
    private String diagnose;
    @SerializedName("complain")
    @Expose
    private String complain;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("totalPrice")
    @Expose
    private Double totalPrice;
    @SerializedName("finalPrice")
    @Expose
    private Double finalPrice;
    @SerializedName("attachment")
    @Expose
    private Object attachment;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("orderMedicines")
    @Expose
    private ArrayList<OrderMedicineModel> orderMedicines = null;
    @SerializedName("pharmacyId")
    @Expose
    private Integer pharmacyId;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("notes")
    @Expose
    private String notes;
    @SerializedName("latitude")
    @Expose
    private Double latitude;
    @SerializedName("longitude")
    @Expose
    private Double longitude;
    @SerializedName("suitableDate")
    @Expose
    private String suitableDate;
    @SerializedName("userData")
    @Expose
    private UserDataModel userData;
    @SerializedName("patientId")
    @Expose
    private Integer patientId;
    @SerializedName("doctorId")
    @Expose
    private Integer doctorId;
    @SerializedName("doctorName")
    @Expose
    private String doctorName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDiagnose() {
        return diagnose;
    }

    public void setDiagnose(String diagnose) {
        this.diagnose = diagnose;
    }

    public String getComplain() {
        return complain;
    }

    public void setComplain(String complain) {
        this.complain = complain;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Double getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(Double finalPrice) {
        this.finalPrice = finalPrice;
    }

    public Object getAttachment() {
        return attachment;
    }

    public void setAttachment(Object attachment) {
        this.attachment = attachment;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public ArrayList<OrderMedicineModel> getOrderMedicines() {
        return orderMedicines;
    }

    public void setOrderMedicines(ArrayList<OrderMedicineModel> orderMedicines) {
        this.orderMedicines = orderMedicines;
    }

    public Integer getPharmacyId() {
        return pharmacyId;
    }

    public void setPharmacyId(Integer pharmacyId) {
        this.pharmacyId = pharmacyId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getSuitableDate() {
        return suitableDate;
    }

    public void setSuitableDate(String suitableDate) {
        this.suitableDate = suitableDate;
    }

    public UserDataModel getUserData() {
        return userData;
    }

    public void setUserData(UserDataModel userData) {
        this.userData = userData;
    }

    public Integer getPatientId() {
        return patientId;
    }

    public void setPatientId(Integer patientId) {
        this.patientId = patientId;
    }

    public Integer getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(Integer doctorId) {
        this.doctorId = doctorId;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}

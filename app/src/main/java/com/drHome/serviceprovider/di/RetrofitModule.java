package com.drHome.serviceprovider.di;


import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.util.Log;


import com.drHome.serviceprovider.BuildConfig;
import com.drHome.serviceprovider.network.ApiService;
import com.drHome.serviceprovider.ui.activities.SplashActivity;
import com.drHome.serviceprovider.utils.AndroidApplication;
import com.drHome.serviceprovider.utils.CommonUtil;
import com.drHome.serviceprovider.utils.Config;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.android.components.ApplicationComponent;
import hu.akarnokd.rxjava3.retrofit.RxJava3CallAdapterFactory;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.Buffer;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import timber.log.Timber;

@Module
@InstallIn(ApplicationComponent.class)
public class RetrofitModule {
    /**
     * This is our main backend/server URL.
     */
    //drhome-1544575261424-38c20
    //drhome-1544575261424-fa675
    //http://drhometst.beetleware.com/doctorhome/
    //https://api.drathome.com.sa/doctorhome/

    public static String REST_API_URL = Config.BaseUrl;


    @Provides
    @Singleton
    public static ApiService apiService() {

        return new Retrofit.Builder()
                .baseUrl(REST_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
                .client(provideOkHttpClient())
                .build().create(ApiService.class);
    }


    private static OkHttpClient provideOkHttpClient() {
        return new OkHttpClient.Builder()
                .addInterceptor(provideSentLanguageInterceptor())
                .addInterceptor(provideLogInterceptor())
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {

                        Request request = chain.request();
                        Response response = chain.proceed(request);
                        Log.d("TAG", "intercept: " + response.code());

                        return response;
                    }
                })
                .connectTimeout(120, TimeUnit.SECONDS)
                .readTimeout(120, TimeUnit.SECONDS)
                .writeTimeout(120, TimeUnit.SECONDS)
                .callTimeout(120, TimeUnit.SECONDS)
                .connectTimeout(120, TimeUnit.SECONDS)
                .build();
    }

    public static Interceptor provideSentLanguageInterceptor() {
        return chain -> {
            if (chain.request().url().toString().contains("oauth")) {
                return chain.proceed(chain.request().newBuilder().addHeader("Content-Type", "application/json").build());
            }
            Request request = chain.request().newBuilder().addHeader("Content-Type", "application/json")
                    .addHeader("Authorization", CommonUtil.getDataFromSharedPref(Config.ACCESS_TOKEN))

                    .build();
            //    Log.d(TAG, "provideSentLanguageInterceptor: "+CommonUtil.commonSharedPref().getString(""+ Config.TOKEN,""));
//            Request request = chain.request().newBuilder().addHeader("Accept-Language"
//                    , CommonUtil.commonSharedPref().getString("appLanguage", "en")
//            ).build();
            //Timber.d("Retrofit :: " + request.headers().toString());
            return chain.proceed(request);
        };


//                chain -> {
//            Request.Builder requestBuilder = chain.request().newBuilder();
//            requestBuilder.removeHeader("Accept-Language");
//            requestBuilder.addHeader("Accept-Language", CommonUtil.commonSharedPref().getString("appLanguage", "en"));
//            return chain.proceed(requestBuilder.build());
//        };
    }

    public static Interceptor provideLogInterceptor() {

        return chain -> {
            Timber.d("TAG: test");
            Request originalRequest = chain.request(); //Current Request

            Response response = chain.proceed(originalRequest); //Get response of the request
            Log.d("TAG", "provideLogInterceptor: //");

            if (response.code() == 401) {
                ActivityManager am = (ActivityManager) AndroidApplication.getAppContext().getSystemService(Context.ACTIVITY_SERVICE);
                ComponentName cn = am.getRunningTasks(1).get(0).topActivity;
                Log.d("TAG", "provideLogInterceptor: " + cn.getClassName());
                if (cn.getClassName().contains("MainActivity")) {

                    CommonUtil.commonSharedPref().edit().clear();
                    CommonUtil.commonSharedPref().edit().commit();
                    CommonUtil.commonSharedPref().edit().remove(Config.BRANCH_ID).commit();
                    AndroidApplication.getAppContext().startActivity(
                            new Intent(AndroidApplication.getAppContext(), SplashActivity.class)
                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                }

            }
            /** DEBUG STUFF */
            if (BuildConfig.DEBUG) {
                Log.d("TAG", "provideLogInterceptor:3 " + BuildConfig.DEBUG);

//            I am logging the response body in debug mode. When I do this I consume the response (OKHttp only lets you do this once) so i have re-build a new one using the cached body
                String bodyString = response.body().string();
                Timber.d(String.format("Sending request %s \n\n with body %s \n\n with headers %s ", originalRequest.url()
                        , (originalRequest.body() != null ? bodyToString(originalRequest) : originalRequest.body()), originalRequest.headers()));
                Timber.d(String.format("Got response HTTP %s %s \n\n with body %s \n\n with headers %s ", response.code(), response.message(), bodyString, response.headers()));
                response = response.newBuilder().body(ResponseBody.create(response.body().contentType(), bodyString)).build();
            }

            return response;
        };
    }

    private static String bodyToString(final Request request) {

        try {
            final Request copy = request.newBuilder().build();
            final Buffer buffer = new Buffer();
            copy.body().writeTo(buffer);
            return buffer.readUtf8();
        } catch (final IOException e) {
            return "did not work";
        }
    }

}